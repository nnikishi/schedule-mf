//
//  NavigationBar.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/21/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "NavigationBar.h"

@implementation NavigationBar

+ (void)applyDesignForNavigationBar:(UINavigationBar *)navigationBar
{
    navigationBar.barTintColor = [AppSettings sharedSettings].navBarColor;
    navigationBar.translucent = NO;
    navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    navigationBar.titleTextAttributes = textAttributes;
}


@end
