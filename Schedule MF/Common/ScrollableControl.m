//
//  ScrollableControl.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/30/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "ScrollableControl.h"

const CGFloat CONST_VERTICALOFFSET = 5.0;

@interface ScrollableControl()

@property (nonatomic, assign) NSInteger selectedIndexPrivate;
@property (nonatomic, strong) NSArray *buttonsArray;
@property (nonatomic, strong) UIView *highlightedView;

@end

@implementation ScrollableControl

#pragma mark Getters
-(NSInteger)selectedIndex
{
    return self.selectedIndexPrivate;
}

#pragma mark Setters

+ (CGRect)frameForHighlightedView:(CGRect)buttonFrame
{
    CGRect newFrame = CGRectMake(CGRectGetMinX(buttonFrame) + CONST_VERTICALOFFSET, CGRectGetMaxY(buttonFrame) - 7.0, CGRectGetWidth(buttonFrame) - CONST_VERTICALOFFSET*2, 3.0);
    
    return newFrame;
    
}

- (UIView *)highlightedView
{
    if (!_highlightedView) {
        UIButton *theButton = [self.buttonsArray objectAtIndex:self.selectedIndex];
        CGRect viewFrame = [ScrollableControl frameForHighlightedView:theButton.frame];
        _highlightedView = [[UIView alloc ] initWithFrame:viewFrame];
        if (self.highlightedViewColor) {
            _highlightedView.backgroundColor = self.highlightedViewColor;
        } else {
            _highlightedView.backgroundColor = [UIColor clearColor];
        }
    }
    
    return _highlightedView;
}

- (void)setHighlightedViewColor:(UIColor *)highlightedViewColor
{
    _highlightedViewColor = highlightedViewColor;
    _highlightedView.backgroundColor = highlightedViewColor;
}

- (void)setControlBackgroundColor:(UIColor *)controlBackgroundColor
{
    _controlBackgroundColor = controlBackgroundColor;
    self.backgroundColor = _controlBackgroundColor;
}
- (void)setButtonBackgroundColor:(UIColor *)buttonBackgroundColor
{
    _buttonBackgroundColor = buttonBackgroundColor;
    
    for (UIButton *theButton in self.buttonsArray) {
        theButton.backgroundColor = _buttonBackgroundColor;
    }
}

#pragma mark designited initiliazer
- (instancetype)initWithFrame:(CGRect)frame items:(NSArray *)items;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = self.controlBackgroundColor;
        NSInteger tag = 0;
        CGFloat nextXCoordinate = 0.;
        
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:[items count]];
        
        for (NSString *item in items) {
            
            UIButton *itemButton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            CGSize textSize = [AppSettings sizeOfMultiLineLabel:item label:itemButton.titleLabel];
            CGFloat widthOffset = CONST_VERTICALOFFSET *2;
            CGRect buttonFrame = CGRectMake(nextXCoordinate, 0, textSize.width + widthOffset, CGRectGetHeight(frame));
            nextXCoordinate = CGRectGetMaxX(buttonFrame);
            [itemButton setTitle:item forState:UIControlStateNormal];
            itemButton.tag = tag;
            itemButton.frame = buttonFrame;
            itemButton.backgroundColor = self.buttonBackgroundColor;
            [itemButton addTarget:self action:@selector(buttonDidTapped:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:itemButton];
            [array addObject:itemButton];
        }
        self.buttonsArray = array;
        self.contentSize = CGSizeMake(nextXCoordinate, CGRectGetHeight(frame));
        [self addSubview:self.highlightedView];
    
    }
    return self;
}


- (void)scrollToItemIndex:(NSInteger)itemIndex
{
    [self scrollToItemIndex:itemIndex animated:NO];
}
- (void)scrollToItemIndex:(NSInteger)itemIndex animated:(BOOL)animated
{
    
   
    
    if (itemIndex<[self.buttonsArray count]) {
        
        UIButton *theButton = [self.buttonsArray objectAtIndex:itemIndex];

        if (itemIndex!=0 && itemIndex!= [self.buttonsArray count]-1) {
            UIButton *theButtonBefore = [self.buttonsArray objectAtIndex:itemIndex-1];
            UIButton *theButtonAfter = [self.buttonsArray objectAtIndex:itemIndex+1];

            CGRect frameToScroll = CGRectMake(CGRectGetMinX(theButtonBefore.frame), 0, CGRectGetWidth(theButtonBefore.frame) + CGRectGetWidth(theButtonAfter.frame) + CGRectGetWidth(theButton.frame), CGRectGetHeight(self.frame));
            
            [self scrollRectToVisible:frameToScroll animated:YES];

        } else {
            [self scrollRectToVisible:theButton.frame animated:YES];

        }
        
        CGRect newFrame = [ScrollableControl frameForHighlightedView:theButton.frame];
        

        if (animated) {
            __weak ScrollableControl *wself = self;
            [UIView animateWithDuration:.3 delay:0.0 options:(UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState) animations:^{
                
                wself.highlightedView.frame = newFrame;
                
            } completion:^(BOOL finished) {
                
                
                
            }];
            
        } else {
            self.highlightedView.frame = newFrame;

        }
        

    }
    
}

- (void)buttonDidTapped:(id)sender
{
    
    UIButton *tappedButton = (UIButton *)sender;
    NSInteger index = [self.buttonsArray indexOfObject:tappedButton];
    
    [self.scrollableControlDelegate scrollableControl:self didTappedButtonAtIndex:index];
    [self scrollToItemIndex:index animated:YES];
}

@end
