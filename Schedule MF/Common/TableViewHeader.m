//
//  TableViewHeader.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/1/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "TableViewHeader.h"

const CGFloat CONST_LABEL_HORIZONTAL_OFFSET = 7.0;

@interface TableViewHeader()

@property (nonatomic, strong) UILabel *captionLabel;

@end


@implementation TableViewHeader

#pragma mark designated initiliazer
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [AppSettings sharedSettings].navBarColor;
    
    }
    return self;
}

#pragma mark Getters

- (UILabel *)captionLabel
{
    if (!_captionLabel) {
        _captionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CONST_LABEL_HORIZONTAL_OFFSET, 0, CGRectGetWidth(self.frame) - CONST_LABEL_HORIZONTAL_OFFSET * 2.0, CGRectGetHeight(self.frame))];
        _captionLabel.backgroundColor = [UIColor clearColor];
        _captionLabel.text = self.captionText;
        _captionLabel.textColor = [UIColor blackColor];
        
        
        [self addSubview:_captionLabel];
    }
    
    return _captionLabel;
}

#pragma mark Setters
- (void)setCaptionText:(NSString *)captionText
{
    _captionText = captionText;
    self.captionLabel.text = captionText;
}

#pragma mark Methods

- (TableViewHeader *)duplicate
{
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:self];
    TableViewHeader * newView = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
    return newView;
}

@end
