//
//  DataManager.m
//  UEFA Fantasy
//
//  Created by Nickolai Nikishin on 1/15/14.
//  Copyright (c) 2014 Releaze. All rights reserved.
//

#import "DataManager.h"

NSString * const DataManagerDidSaveNotification = @"DataManagerDidSaveNotification";
NSString * const DataManagerDidSaveFailedNotification = @"DataManagerDidSaveFailedNotification";

@interface DataManager()


- (NSString*)sharedDocumentsPath;


@end

@implementation DataManager

NSString * const kDataManagerBundleName = nil;
NSString * const kDataManagerModelName = @"cd_store";
NSString * const kDataManagerSQLiteName = @"cd_store.sqlite";
NSSearchPathDirectory const kDirectory = NSLibraryDirectory;
NSString * const kSubDirectoryName = @"Storage";

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize mainObjectContext = _mainObjectContext;
@synthesize objectModel = _objectModel;
@synthesize privateObjectContext = _privateObjectContext;

+ (DataManager*)sharedManager {
	static dispatch_once_t pred;
	static DataManager *sharedManager = nil;
    
	dispatch_once(&pred, ^{ sharedManager = [[self alloc] init];
    
    
    });
	return sharedManager;
}

- (instancetype)init{
    self = [super init];
    
    if (self) {
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(mergeChanges:)
                   name:NSManagedObjectContextDidSaveNotification
                 object:nil];

    }
    
    return self;
}

- (void)dealloc {
	
    [[NSNotificationCenter defaultCenter] removeObserver:self];
   
}


- (void)mergeChanges:(NSNotification *)notification
{
    if ([notification object] != self.mainObjectContext &&
                                  [notification object] != self.privateObjectContext)
        return;
    

    
    NSManagedObjectContext *moc = [notification object];

    
    if (moc.persistentStoreCoordinator!=self.persistentStoreCoordinator) {
   
        return;
    }
    
    if (moc == self.mainObjectContext) {
       // DLog(@"Starting merging privateContext");

       //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{


             [self.privateObjectContext mergeChangesFromContextDidSaveNotification:notification];
            // DLog(@"Done merging privateContext");

         });
        
    } else if (moc == self.privateObjectContext) {
        //DLog(@"Starting merging mainContext");

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.mainObjectContext mergeChangesFromContextDidSaveNotification:notification];
           // DLog(@"Done merging mainContext");
        });
        
    }
    

   
}


- (void)saveAllManagedObjectContexts
{
    [self saveManagedObjectContextChanges:_mainObjectContext];
    [self saveManagedObjectContextChanges:_privateObjectContext];
}

- (void)saveManagedObjectContextChanges:(NSManagedObjectContext *)context {
    
    if (context && [context hasChanges]) {
        [context performBlockAndWait:^{
            
            NSError *error = nil;
            if (![context save:&error]) {
                
                NSLog(@"error saving core data: %@", [error localizedDescription]);
            }
            
            
        }];
    }
}

- (NSManagedObjectModel*)objectModel {
	if (_objectModel) {
		return _objectModel;
    }
    
    
	NSBundle *bundle = [NSBundle mainBundle];
	if (kDataManagerBundleName) {
		NSString *bundlePath = [[NSBundle mainBundle] pathForResource:kDataManagerBundleName ofType:@"bundle"];
		bundle = [NSBundle bundleWithPath:bundlePath];
	}
	NSString *modelPath = [bundle pathForResource:kDataManagerModelName ofType:@"momd"];
	_objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL fileURLWithPath:modelPath]];
    
	return _objectModel;
}

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator {
	if (_persistentStoreCoordinator)
		return _persistentStoreCoordinator;
    
	NSString *storePath = [[self sharedDocumentsPath] stringByAppendingPathComponent:kDataManagerSQLiteName];
	NSURL *storeURL = [NSURL fileURLWithPath:storePath];
    
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                             nil];
    
	// Attempt to load the persistent store
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.objectModel];
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:options
                                                           error:&error]) {
		NSLog(@"Fatal error while creating persistent store: %@", error);
		abort();
	}
    
	return _persistentStoreCoordinator;
}

- (NSManagedObjectContext*)mainObjectContext {
	if (_mainObjectContext)
		return _mainObjectContext;
    
	if (![NSThread isMainThread]) {
		[self performSelectorOnMainThread:@selector(mainObjectContext)
                               withObject:nil
                            waitUntilDone:YES];
		return _mainObjectContext;
	}
    
	_mainObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	[_mainObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    
	return _mainObjectContext;
}


- (NSManagedObjectContext*)privateObjectContext {
	if (_privateObjectContext) {
		return _privateObjectContext;
    }
    
    	if (![NSThread isMainThread]) {
    		[self performSelectorOnMainThread:@selector(privateObjectContext)
                                   withObject:nil
                                waitUntilDone:YES];
    		return _privateObjectContext;
        }
    
	_privateObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	[_privateObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    
	return _privateObjectContext;
}

- (NSString*)sharedDocumentsPath {
    
	static NSString *sharedDocumentsPathStr = nil;
	if (sharedDocumentsPathStr)
		return sharedDocumentsPathStr;
    
	NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(kDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	sharedDocumentsPathStr = [libraryPath stringByAppendingPathComponent:kSubDirectoryName];
    
	NSFileManager *manager = [NSFileManager defaultManager];
	BOOL isDirectory;
	if (![manager fileExistsAtPath:sharedDocumentsPathStr isDirectory:&isDirectory] || !isDirectory) {
		NSError *error = nil;
		NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
		[manager createDirectoryAtPath:sharedDocumentsPathStr
		   withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
		if (error)
			NSLog(@"Error creating directory path: %@", [error localizedDescription]);
	}
    
	return sharedDocumentsPathStr;
}

#pragma mark Common Method

+ (NSNumber *)numberFromBOOLString:(NSString *)string
{
    return [NSNumber numberWithBool:[string boolValue]];
}


+ (NSNumber *)numberFromIntegerString:(NSString *)string
{
    
    NSNumber *theNumber = [NSNumber numberWithInteger:[string integerValue]];
    
//    if ([theNumber isEqual:[NSNull null]]) {
//        theNumber = @0;
//    }

    
    return theNumber;
   
}
+ (NSNumber *)numberFromDoubleString:(NSString *)string
{
     NSNumber *theNumber = [NSNumber numberWithDouble:[string doubleValue]];
    
//    if ([theNumber isEqual:[NSNull null]]) {
//        theNumber = @0;
//    }
    
    return theNumber;

}

+ (NSDateFormatter*)dateFromStringFormatter
{
    static NSDateFormatter* dateFromStringFormatter = nil;
    if (dateFromStringFormatter == nil)
    {
        dateFromStringFormatter = [[NSDateFormatter alloc] init];
        
        [dateFromStringFormatter setDateFormat:@"yyyyMMddHHmm"];
        
        NSTimeZone *zone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];
        
        
        [dateFromStringFormatter setTimeZone:zone];
    }
    return dateFromStringFormatter;
}

+ (NSDateFormatter*)dateFromTwitterStringFormatter
{
    static NSDateFormatter* dateFromTwitterStringFormatter = nil;
    if (dateFromTwitterStringFormatter == nil)
    {
        dateFromTwitterStringFormatter = [[NSDateFormatter alloc] init];
        
        [dateFromTwitterStringFormatter setDateFormat:@"EEE MMM dd HH:mm:ss ZZZZ yyyy"];
        
    }
    return dateFromTwitterStringFormatter;
}

+ (NSDateFormatter*)stringFromDateFormatter
{
    static NSDateFormatter* stringFromDateFormatter = nil;
    if (stringFromDateFormatter == nil)
    {
        stringFromDateFormatter = [[NSDateFormatter alloc] init];
        [stringFromDateFormatter setDateFormat:@"yyyyMMddHHmm"];
        NSTimeZone *zone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];
        [stringFromDateFormatter setTimeZone:zone];
    }
    return stringFromDateFormatter;
}

+ (NSDateFormatter*)humanStringFromDateFormatter
{
    static NSDateFormatter* humanStringFromDateFormatter = nil;
    if (humanStringFromDateFormatter == nil)
    {
        humanStringFromDateFormatter = [[NSDateFormatter alloc] init];
        [humanStringFromDateFormatter setDateFormat:@"dd MMM HH:mm"];
        NSTimeZone *zone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];
        [humanStringFromDateFormatter setTimeZone:zone];
    }
    return humanStringFromDateFormatter;
}

+ (NSDateFormatter*)humanStringFromTimeFormatter
{
    static NSDateFormatter* humanStringFromTimeFormatter = nil;
    if (humanStringFromTimeFormatter == nil)
    {
        humanStringFromTimeFormatter = [[NSDateFormatter alloc] init];
        [humanStringFromTimeFormatter setDateFormat:@"HH:mm"];
        NSTimeZone *zone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];
        [humanStringFromTimeFormatter setTimeZone:zone];
    }
    return humanStringFromTimeFormatter;
}

+ (NSDateFormatter*)humanStringFromDateWithoutTimeFromDateFormatter
{
    static NSDateFormatter* humanStringFromDateWithoutTimeFromDateFormatter = nil;
    if (humanStringFromDateWithoutTimeFromDateFormatter == nil)
    {
        humanStringFromDateWithoutTimeFromDateFormatter = [[NSDateFormatter alloc] init];
        [humanStringFromDateWithoutTimeFromDateFormatter setDateFormat:@"dd MMM yyyy"];
        NSTimeZone *zone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];
        [humanStringFromDateWithoutTimeFromDateFormatter setTimeZone:zone];
    }
    return humanStringFromDateWithoutTimeFromDateFormatter;
}

+ (NSDateFormatter*)stringByRemovingTimeComponentInDateStringFromDateFormatter
{
    static NSDateFormatter* stringByRemovingTimeComponentInDateStringFromDateFormatter = nil;
    if (stringByRemovingTimeComponentInDateStringFromDateFormatter == nil)
    {
        stringByRemovingTimeComponentInDateStringFromDateFormatter = [[NSDateFormatter alloc] init];
        [stringByRemovingTimeComponentInDateStringFromDateFormatter setDateFormat:@"yyyyMMdd"];
        NSTimeZone *zone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];
        [stringByRemovingTimeComponentInDateStringFromDateFormatter setTimeZone:zone];
    }
    return stringByRemovingTimeComponentInDateStringFromDateFormatter;
}

+ (NSDate *)dateFromString:(NSString *)string
{
    NSDate *theDate = [[self dateFromStringFormatter] dateFromString:string];
    
    if (!theDate) {
        theDate = (NSDate *)[NSNull null];
        NSLog(@"CANNOT CONVERT DATE PROBLEM FROM STRING %@", string);
    }
    
    return theDate;
    
}

+ (NSDateFormatter*)stringFromTwitterDateFormatter
{
    static NSDateFormatter* stringFromTwitterDateFormatter = nil;
    if (stringFromTwitterDateFormatter == nil)
    {
        stringFromTwitterDateFormatter = [[NSDateFormatter alloc] init];
        stringFromTwitterDateFormatter.locale = [NSLocale autoupdatingCurrentLocale];
        stringFromTwitterDateFormatter.timeStyle = NSDateFormatterMediumStyle;
        stringFromTwitterDateFormatter.dateStyle = NSDateFormatterShortStyle;
        stringFromTwitterDateFormatter.doesRelativeDateFormatting = YES;
    }
    return stringFromTwitterDateFormatter;
}



+ (NSDate *)dateFromTwitterString:(NSString *)string
{
    return [[self dateFromTwitterStringFormatter] dateFromString:string];
    
}
+ (NSString *)stringFromDate:(NSDate *)date
{
    return [[self stringFromDateFormatter] stringFromDate:date];

}
+ (NSString *)humanStringFromDate:(NSDate *)date
{
  //  NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];
    return [NSString stringWithFormat:@"%@ %@", [[self humanStringFromDateFormatter] stringFromDate:date], NSLocalizedString(@"CET_code", nil)];
    
}

+ (NSString *)humanStringFromTwitterDate:(NSDate *)date
{
       return [[self stringFromTwitterDateFormatter] stringFromDate:date];
    
}


+ (NSString *)humanStringFromTime:(NSDate *)date
{
    return [[self humanStringFromTimeFormatter] stringFromDate:date];
    
}

+ (NSString *)humanStringFromDateWithoutTime:(NSDate *)date
{
    return [[self humanStringFromDateWithoutTimeFromDateFormatter] stringFromDate:date];
    
}


+ (NSString *)stringByRemovingTimeComponentInDateString:(NSString *)stringdate
{
    NSDate *theDate = [self dateFromString:stringdate];
    return [[self stringByRemovingTimeComponentInDateStringFromDateFormatter] stringFromDate:theDate];
}

+ (NSDate *)dateWithoutTime:(NSDate *)datDate {
    
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:datDate];
    NSTimeZone *zone = [NSTimeZone timeZoneWithAbbreviation:@"CET"];

    [comps setTimeZone:zone];
    [comps setHour:00];
    [comps setMinute:00];
    [comps setSecond:00];
    
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

#pragma mark fetchmethods

+ (NSArray *)fetchDataStore:(NSManagedObjectContext *)context entity:(NSString *)entity predicate:(NSPredicate *)predicate
{
    
    __block NSArray *fetchedItems = nil;
    
    [context performBlockAndWait:^(){
        NSError *error = nil;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *ent = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
        fetchRequest.entity = ent;
        fetchRequest.predicate = predicate;
        fetchedItems = [context executeFetchRequest:fetchRequest error:&error];
        
        
        if (error) {
            NSLog(@"Unresolved error when fetching object: %@ predicate: %@ error: %@, %@", entity, [predicate predicateFormat],  error, [error userInfo]);
        }

    }];
    
    return fetchedItems;
    
   
}

+ (NSArray *)fetchDataStore:(NSManagedObjectContext *)context entity:(NSString *)entity predicate:(NSPredicate *)predicate sortDecriptorsArray:(NSArray *)sortDescriptorsArray
{
    
    __block NSArray *fetchedItems = nil;
    
    [context performBlockAndWait:^(){
        NSError *error = nil;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *ent = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
        fetchRequest.entity = ent;
        fetchRequest.predicate = predicate;
        fetchRequest.sortDescriptors = sortDescriptorsArray;
        fetchedItems = [context executeFetchRequest:fetchRequest error:&error];
        
        
        if (error) {
            NSLog(@"Unresolved error when fetching object: %@ predicate: %@ error: %@, %@", entity, [predicate predicateFormat],  error, [error userInfo]);
        }
        
    }];
    
    return fetchedItems;
    
    
}

+ (NSArray *)fetchDistinctValues:(NSString *)property context:(NSManagedObjectContext *) context entity:(NSString *)entity predicate:(NSPredicate *)predicate sortDecriptorsArray:(NSArray *)sortDescriptorsArray
{
    __block NSArray *fetchedItems = nil;

    [context performBlockAndWait:^() {

    NSEntityDescription *coredataEntity = [NSEntityDescription  entityForName:entity inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:coredataEntity];
    [request setResultType:NSDictionaryResultType];
    [request setReturnsDistinctResults:YES];
    [request setPropertiesToFetch:@[property]];
    
    NSError *error;
    fetchedItems = [context executeFetchRequest:request error:&error];
    
    if (error) {
        DLog(@"Error reading datastore for fetching distinct property %@", [error localizedDescription]);
    }

    }];
    
    return fetchedItems;
}

+ (NSString *)valueInString:(NSString *)string forKey:(NSString *)key
{
    NSString *returnString = @"";
    
    if (key) {
        NSArray *tempArray = [string componentsSeparatedByString:[NSString stringWithFormat:@"%@=", key]];
        
        if ([tempArray count]>1) {
            NSString *tempString = [tempArray objectAtIndex:1];
            
            NSArray *tempArray = [tempString componentsSeparatedByString:@"\""];
            if ([tempArray count]>1) {
                
                returnString = [tempArray objectAtIndex:1];
            }
            
            
        }
        
    }
    
    return returnString;
}


@end
