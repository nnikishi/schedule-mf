//
//  AppSettings.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define iOS7_0 @"7.0"

#define IS_IPHONE5  [[UIScreen mainScreen] bounds].size.height == 568 ? YES:NO

//notifications

#define NOTIFICATION_DID_RECEIVE_SETTINGS @"Notification_DidReceiveSettings"
#define NOTIFICATION_DID_UPDATE_FAVORITES @"Notification_DidUpdateFavorites"

@class TableObject;

@interface AppSettings : NSObject

+ (AppSettings *)sharedSettings;



+ (BOOL)isiPad;
+ (BOOL)isiOS7;
+ (UIStoryboard *)mainStoryboard;
+ (NSInteger)currentDay;

- (NSOperation *)getSettingsWithSucessBlock:(void (^)())sucessBlock
                                 errorBlock:(void (^)(NSError *error))errorBlock;


@property (nonatomic, strong) NSURL *feedURL;
@property (nonatomic, strong) NSArray *weekdaysArray;

@property (nonatomic, strong) NSString *groupExercisesHeader;
@property (nonatomic, strong) NSArray  *groupExercisesArray;

@property (nonatomic, strong) NSString *childrenExercisesHeader;
@property (nonatomic, strong) NSArray  *childrenExercisesArray;

@property (nonatomic, strong) NSString *aboutHeader;
@property (nonatomic, strong) NSArray  *aboutRowsArray;

@property (nonatomic, strong) NSString *aboutAppHeader;
@property (nonatomic, strong) NSArray *aboutAppRowsArray;

@property (nonatomic, strong) TableObject *menuTable;
@property (nonatomic, strong) TableObject *workingTimeTable;



@property (nonatomic, strong) UIColor *navBarColor;
@property (nonatomic, assign) BOOL isFlurryAlreadyStarted;



+ (CGSize)sizeOfMultiLineLabel:(NSString *)text label:(UILabel *)label;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (NSString *)extactPhoneNumberToDialFromString:(NSString *)phoneNumberString error:(BOOL *)error;
- (void)startFlurryIfNeeded;

@end
