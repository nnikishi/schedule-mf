//
//  TableViewHeader.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/1/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewHeader : UIView

@property (nonatomic, strong) NSString *captionText;

- (TableViewHeader *)duplicate;

@end
