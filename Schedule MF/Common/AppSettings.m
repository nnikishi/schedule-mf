//
//  AppSettings.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "AppSettings.h"
#import "AFNetworking.h"
#import "TableObject.h"
#import "Flurry.h"


#define CONST_SETTINGS_URL @"https://spreadsheets.google.com/feeds/list/1mbUsbusvYii4oZsfweAjE9j8x-pYYhgn9H5edgqnrp0/od6/public/values?alt=json"
#define CONST_PROJECT_PREFIX @"MF"
#define CONST_SETTINGS @"settings"


@interface AppSettings()

@property (nonatomic, strong) NSDictionary *settingsRaw;
@property (nonatomic, strong) NSDictionary *settings;

@property (nonatomic, strong) NSString  *childrenExercises;
@property (nonatomic, strong) NSString  *groupExercises;
@property (nonatomic, strong) NSString  *aboutRows;
@property (nonatomic, strong) NSString  *aboutAppRows;

@end

@implementation AppSettings

@synthesize settings = _settings;
@dynamic feedURL;

@dynamic groupExercises;
@dynamic groupExercisesArray;
@dynamic groupExercisesHeader;

@dynamic childrenExercises;
@dynamic childrenExercisesArray;
@dynamic childrenExercisesHeader;

@dynamic aboutRows;
//@dynamic aboutRowsArray;
@dynamic aboutHeader;

@dynamic aboutAppRows;
//@dynamic aboutAppRowsArray;
@dynamic aboutAppHeader;
@dynamic navBarColor;

#pragma mark public getters

- (NSArray *)weekdaysArray
{
    if (!_weekdaysArray) {
        NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setLocale: [NSLocale currentLocale]];
        NSArray *weekDays = [df weekdaySymbols];
        NSMutableArray *weekDaysNew = [NSMutableArray arrayWithArray:weekDays];
        [weekDaysNew removeObjectAtIndex:0];
        [weekDaysNew addObject:[weekDays firstObject]];
        _weekdaysArray = [NSArray arrayWithArray:weekDaysNew];
    }
    
    return _weekdaysArray;
}


#pragma getters

- (TableObject *)menuTable
{
    if (!_menuTable) {
        _menuTable = [[TableObject alloc] initWithSettings:self.settings tableName:@"menuTable"];
    }
    
    return _menuTable;
}

- (TableObject *)workingTimeTable
{
    if (!_workingTimeTable) {
        _workingTimeTable = [[TableObject alloc] initWithSettings:self.settings tableName:@"workingTimeTable"];
    }
    
    return _workingTimeTable;
}

- (NSDictionary *)settings
{
    if (!_settings) {
        NSDictionary *settingsDic = self.settingsRaw;
        if (settingsDic) {
            NSArray *propertiesArray = [[settingsDic objectForKey:@"feed"] objectForKey:@"entry"];
            NSMutableDictionary *propertiesDic = [[NSMutableDictionary alloc] init];
            
            for (NSDictionary *propertyDic in propertiesArray) {
                
                NSString *key = [[propertyDic objectForKey:@"gsx$key"] objectForKey:@"$t"];
                NSString *value = [[propertyDic objectForKey:@"gsx$value"] objectForKey:@"$t"];
                
                if (key && value) {
                    [propertiesDic setObject:value forKey:key];
                }
                
            }
            
            _settings = propertiesDic;
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:propertiesDic forKey:CONST_SETTINGS];
            [userDefaults synchronize];
            

        
        } else {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            _settings = [userDefaults objectForKey:CONST_SETTINGS];
        }
        
    }
    
    return _settings;
}

#pragma mark setters
- (void)setSettingsRaw:(NSDictionary *)settingsRaw
{
    if (settingsRaw) {
        
        _settingsRaw = settingsRaw;
        _settings = nil;
        _aboutAppRowsArray = nil;
        _aboutRowsArray = nil;
        _menuTable = nil;
        _workingTimeTable = nil;
        [self startFlurryIfNeeded];

        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DID_RECEIVE_SETTINGS object:nil userInfo:nil];
    }
}

+ (AppSettings *)sharedSettings
{
	static dispatch_once_t pred;
	static AppSettings *sharedSettings = nil;
    
	dispatch_once(&pred, ^{ sharedSettings = [[self alloc] init];
        
        
    });
	return sharedSettings;
}

+ (BOOL)isiPad
{
    
    UIDevice *device = [UIDevice currentDevice];
    
    if ([device userInterfaceIdiom]==UIUserInterfaceIdiomPad) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isiOS7
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        return YES;
    }
    return NO;
}

+ (UIStoryboard *)mainStoryboard
{
    if ([self isiPad]) {
        NSString *storyBoardNameiPad = [NSString stringWithFormat:@"%@.iPad", CONST_PROJECT_PREFIX];
        return [UIStoryboard storyboardWithName:storyBoardNameiPad bundle:nil];
        
    }
    NSString *storyBoardNameiPhone = [NSString stringWithFormat:@"%@.iPhone", CONST_PROJECT_PREFIX];

    return [UIStoryboard storyboardWithName:storyBoardNameiPhone bundle:nil];
}

#pragma mark methods
+ (void)updateSettings
{
    [[AppSettings sharedSettings] updateSettings];
}

- (void)updateSettings
{
    [self getSettingsWithSucessBlock:NULL errorBlock:NULL];
}

- (NSOperation *)getSettingsWithSucessBlock:(void (^)())sucessBlock
                                 errorBlock:(void (^)(NSError *error))errorBlock
{
    NSURL *url = [NSURL URLWithString:CONST_SETTINGS_URL];
    
    NSAssert(url, @"problem with URL");
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            self.settingsRaw = (NSDictionary *)responseObject;
        }
        
        sucessBlock();
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [operation start];
    
    return operation;
}

#pragma mark Custom properties

- (NSURL *)feedURL
{
    NSURL *urlToReturn = nil;
    NSString *urlStr = [self.settings objectForKey:@"feedURL"];
    if (urlStr) {
        urlToReturn = [NSURL URLWithString:urlStr];
    }
    
    return urlToReturn;
}

#pragma mark Menu Items

- (NSString *)groupExercises
{
    NSString *groupExercises = [self.settings objectForKey:@"groupExercises"];
    return groupExercises;
}

- (NSString *)childrenExercises
{
    NSString *childrenExercises = [self.settings objectForKey:@"childrenExercises"];
    return childrenExercises;
}

- (NSString *)aboutRows
{
    NSString *aboutRows = [self.settings objectForKey:@"aboutRows"];
    return aboutRows;
}

- (NSString *)aboutAppRows
{
    NSString *aboutAppRows = [self.settings objectForKey:@"aboutAppRows"];
    return aboutAppRows;
}

- (NSArray *)groupExercisesArray
{
    NSString *groupExercises = [self groupExercises];
    
    if ([groupExercises length]>0) {
        NSArray *groupExercisesArray = [groupExercises componentsSeparatedByString:@";"];
        return groupExercisesArray;
    }
   
    return nil;
}

- (NSArray *)childrenExercisesArray
{
    NSString *childrenExercises = [self childrenExercises];
    
    if ([childrenExercises length]>0) {
        NSArray *childrenExercisesArray = [childrenExercises componentsSeparatedByString:@";"];
        return childrenExercisesArray;
    }
    
    return nil;
}

- (NSArray *)aboutRowsArray
{
    if (!_aboutRowsArray) {
        NSString *aboutRows = [self aboutRows];
        
        if ([aboutRows length]>0) {
            NSArray *aboutRowsArray = [aboutRows componentsSeparatedByString:@";"];
            
            NSMutableArray *aboutRowsArrayFinal = [[NSMutableArray alloc] init];
            
            for (NSString *aboutRow in aboutRowsArray) {
                NSString *newRow = [self.settings objectForKey:aboutRow];
                if (newRow) {
                    [aboutRowsArrayFinal addObject:newRow];
                }
            }
            
            _aboutRowsArray = aboutRowsArrayFinal;
            
        }

    }
    
    return _aboutRowsArray;
}

- (NSArray *)aboutAppRowsArray
{
    if (!_aboutAppRowsArray) {
        NSString *aboutAppRows = [self aboutAppRows];
        
        if ([aboutAppRows length]>0) {
            NSArray *aboutAppRowsArray = [aboutAppRows componentsSeparatedByString:@";"];
            
            NSMutableArray *aboutAppRowsArrayFinal = [[NSMutableArray alloc] init];
            
            for (NSString *aboutAppRow in aboutAppRowsArray) {
                NSString *newRow = [self.settings objectForKey:aboutAppRow];
                if (newRow) {
                    [aboutAppRowsArrayFinal addObject:newRow];
                }
            }
            
            _aboutAppRowsArray = aboutAppRowsArrayFinal;
            
        }
        
    }
    
    return _aboutAppRowsArray;
}

- (NSString *)groupExercisesHeader
{
    NSString *groupExercisesHeader = [self.settings objectForKey:@"groupExercisesHeader"];
    return groupExercisesHeader;
}

- (NSString *)childrenExercisesHeader
{
    NSString *childrenExercisesHeader = [self.settings objectForKey:@"childrenExercisesHeader"];
    return childrenExercisesHeader;
}

- (NSString *)aboutHeader
{
    NSString *aboutHeader = [self.settings objectForKey:@"aboutHeader"];
    return aboutHeader;
}

- (NSString *)aboutAppHeader
{
    NSString *aboutAppHeader = [self.settings objectForKey:@"aboutAppHeader"];
    return aboutAppHeader;
}

#pragma mark Colors
- (UIColor *)navBarColor
{
    NSString *navBarColorStr = [self.settings objectForKey:@"navBarColor"];
    
    if ([navBarColorStr length]) {
        UIColor *navBarColor = [AppSettings colorFromHexString:navBarColorStr];
        return navBarColor;
    }
    
    return nil;
}

- (NSString *)flurryKey
{
    NSDictionary *archivedSettings = _settings;
    if (!_settings) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        archivedSettings = [userDefaults objectForKey:CONST_SETTINGS];
    }
    
    NSString *flurryKey = [archivedSettings objectForKey:@"flurryKey"];
    return flurryKey;
}

#pragma mark Common Methods

+ (CGSize)sizeOfMultiLineLabel:(NSString *)text label:(UILabel *)label
{
    
    CGFloat aLabelSizeWidth = CGRectGetWidth(label.frame);
    
    
    if (SYSTEM_VERSION_LESS_THAN(iOS7_0)) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
        return [text sizeWithFont:label.font
                constrainedToSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                    lineBreakMode:NSLineBreakByWordWrapping];
#pragma GCC diagnostic pop
        
    }
    else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(iOS7_0)) {
        
        return [text boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:label.font}
                                  context:nil].size;
        
    }
    
    return [label bounds].size;
    
}

+ (UIColor *)colorFromHexString:(NSString *)hexString
{
	NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha = .0, red = .0, blue = .0, green = .0;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom:colorString start:0 length: 1];
            green = [self colorComponentFrom:colorString start: 1 length: 1];
            blue  = [self colorComponentFrom:colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom:colorString start: 0 length: 1];
            red   = [self colorComponentFrom:colorString start: 1 length: 1];
            green = [self colorComponentFrom:colorString start: 2 length: 1];
            blue  = [self colorComponentFrom:colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom:colorString start: 0 length: 2];
            green = [self colorComponentFrom:colorString start: 2 length: 2];
            blue  = [self colorComponentFrom:colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom:colorString start: 0 length: 2];
            red   = [self colorComponentFrom:colorString start: 2 length: 2];
            green = [self colorComponentFrom:colorString start: 4 length: 2];
            blue  = [self colorComponentFrom:colorString start: 6 length: 2];
            break;
        default:
            DLog(@"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString);
            return [UIColor clearColor];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
    
}

+ (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

+ (NSInteger)currentDay
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSInteger weekday = [comps weekday];
    
    if (weekday==1) {
        weekday = 6;
    } else {
        weekday = weekday-2;
    }
    
    return weekday;
}

+ (NSString *)extactPhoneNumberToDialFromString:(NSString *)phoneNumber error:(BOOL *)error
{
    BOOL errorBool = *error;
    errorBool = NO;
    
    if (phoneNumber) {
        NSMutableString *strippedString = [NSMutableString
                                           stringWithCapacity:phoneNumber.length];
        
        NSScanner *scanner = [NSScanner scannerWithString:phoneNumber];
        NSCharacterSet *numbers = [NSCharacterSet
                                   characterSetWithCharactersInString:@"0123456789"];
        
        while ([scanner isAtEnd] == NO) {
            NSString *buffer;
            if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
                [strippedString appendString:buffer];
                
            } else {
                [scanner setScanLocation:([scanner scanLocation] + 1)];
            }
        }
        
    
        
        if ( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:+%@", strippedString]]]) {
            
            NSString *realPhoneNumber = [NSString stringWithFormat:@"tel:+%@", strippedString];
            return realPhoneNumber;
            
        } else {
            
            errorBool = YES;
        }
        
        
    } else {
        errorBool = YES;
        
    }
    
    return nil;
}


- (void)startFlurryIfNeeded
{
    NSString *fluryKey = [self flurryKey];
    if ([fluryKey length]>0 && ![self isFlurryAlreadyStarted]) {
        
        [Flurry setCrashReportingEnabled:YES];
        [Flurry startSession:fluryKey];
        self.isFlurryAlreadyStarted = YES;
        
    }
}
@end
