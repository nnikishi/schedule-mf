//
//  TableObject.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/15/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TableRow;

@interface TableObject : NSObject
@property (nonatomic, strong) NSArray *tableSections;

- (instancetype)initWithSettings:(NSDictionary *)settings tableName:(NSString *)tableName;
+ (NSDictionary *)dictionaryFromStringSettings:(NSString *)stringSettings;
- (TableRow *)tableRowForIndexPath:(NSIndexPath *)indexPath;

@end



@interface TableSection : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray *tableRows;

- (instancetype)initWithSettings:(NSDictionary *)settings sectionName:(NSString *)sectionName;
@end

@interface TableRow : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL preserveHighline;
+ (TableRow *)tableRowWithSettings:(NSDictionary *)settings rowName:(NSString *)rowName;
@end

@interface TableRowWorkingTime : TableRow
@property (nonatomic, strong) NSArray *workingTimeArray;
@property (nonatomic, strong) NSString *fontColorStr;
@end

@interface TableRowOpenController : TableRow
@property (nonatomic, strong) NSString *controller;
@property (nonatomic, strong) NSString *parameter;
@end

@interface TableRowOpenURL : TableRow
@property (nonatomic, strong) NSString *urlStr;
@property (nonatomic, assign) BOOL openInternally;
@end


@interface TableRowOpenFacebookURL : TableRow
@property (nonatomic, strong) NSArray *urlArray;
@end

@interface TableRowDialNumber : TableRow
@property (nonatomic, strong) NSArray *numbersToDial;
@end

@interface TableRowSendMail : TableRow
@property (nonatomic, strong) NSString *email;
@end