//
//  ShareURLActivitySafari.m
//  TV2 Generic Sportcenter
//
//  Created by Nickolai Nikishin on 5/12/14.
//  Copyright (c) 2014 Releaze A/S. All rights reserved.
//

#import "ShareURLActivitySafari.h"

@implementation ShareURLActivitySafari

- (NSString *)activityTitle {
	return NSLocalizedString(@"Open in Safari", nil);
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
	for (id activityItem in activityItems) {
		if ([activityItem isKindOfClass:[NSURL class]] && [[UIApplication sharedApplication] canOpenURL:activityItem]) {
			return YES;
		}
	}
	return NO;
}

- (void)performActivity {
	BOOL completed = [[UIApplication sharedApplication] openURL:self.URLToOpen];
	[self activityDidFinish:completed];
}


@end
