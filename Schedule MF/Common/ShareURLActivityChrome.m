//
//  ShareURLActivityChrome.m
//  TV2 Generic Sportcenter
//
//  Created by Nickolai Nikishin on 5/12/14.
//  Copyright (c) 2014 Releaze A/S. All rights reserved.
//

#import "ShareURLActivityChrome.h"

@implementation ShareURLActivityChrome

- (NSString *)schemePrefix {
    return @"googlechrome://";
}

- (NSString *)activityTitle {
    return NSLocalizedString(@"Open in Chrome", nil);
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
	for (id activityItem in activityItems) {
		if ([activityItem isKindOfClass:[NSURL class]] && [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.schemePrefix]]) {
			return YES;
		}
	}
	return NO;
}

- (void)performActivity {
    
    NSURL *inputURL = self.URLToOpen;
    NSString *scheme = inputURL.scheme;
    NSString *chromeScheme = nil;
    if ([scheme isEqualToString:@"http"]) {
        chromeScheme = @"googlechrome";
    } else if ([scheme isEqualToString:@"https"]) {
        chromeScheme = @"googlechromes";
    }
    
//    NSString *openingURL = [self.URLToOpen.absoluteString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSURL *activityURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", self.schemePrefix, openingURL]];
//	[[UIApplication sharedApplication] openURL:activityURL];
    
    if (chromeScheme) {
        NSString *absoluteString = [inputURL absoluteString];
        NSRange rangeForScheme = [absoluteString rangeOfString:@":"];
        NSString *urlNoScheme =
        [absoluteString substringFromIndex:rangeForScheme.location];
        NSString *chromeURLString =
        [chromeScheme stringByAppendingString:urlNoScheme];
        NSURL *chromeURL = [NSURL URLWithString:chromeURLString];
        
        [[UIApplication sharedApplication] openURL:chromeURL];
    }
    
	[self activityDidFinish:YES];
}

@end
