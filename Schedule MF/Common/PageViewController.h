//
//  PageViewController.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/10/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIPageViewController

- (void)setViewControllers:(NSArray *)viewControllers direction:(UIPageViewControllerNavigationDirection)direction invalidateCache:(BOOL)invalidateCache animated:(BOOL)animated completion:(void (^)(BOOL finished))completion;

@end
