//
//  PortraitViewController.m
//  UEFA Fantasy
//
//  Created by Nickolai Nikishin on 12/24/13.
//  Copyright (c) 2013 Releaze. All rights reserved.
//

#import "PortraitViewController.h"

@interface PortraitViewController ()

@end

@implementation PortraitViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self setNeedsStatusBarAppearanceUpdate];

    }
    
 }

- (void)viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (BOOL)shouldAutorotate
{
    if ([AppSettings isiPad]) {
        return YES;
    }
    
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{

    if ([AppSettings isiPad]) {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);

    }
    
    return UIInterfaceOrientationMaskPortrait;


}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
   

    return UIInterfaceOrientationPortrait;
}



-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

//- (void) moveAllSubviewsDown{
//    float barHeight = 45.0;
// //   for (UIView *view in self.view.subviews) {
//    UIView *view = self.view;
//        
//        if ([view isKindOfClass:[UIScrollView class]]) {
//            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - barHeight, view.frame.size.width, view.frame.size.height - barHeight);
//        } else {
//            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - barHeight, view.frame.size.width, view.frame.size.height);
//        }
//   // }
//}
@end
