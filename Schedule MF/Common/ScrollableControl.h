//
//  ScrollableControl.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/30/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScrollableControlDelegate <NSObject>

@optional
- (void)scrollableControl:(id)sender didTappedButtonAtIndex:(NSInteger)buttonIndex;

@end

@interface ScrollableControl : UIScrollView

@property (nonatomic, weak) id<ScrollableControlDelegate> scrollableControlDelegate;

@property (nonatomic, strong) UIColor *highlightedViewColor;
@property (nonatomic, strong) UIColor *defaultFontColor;
@property (nonatomic, strong) UIColor *highlightedFontColor;
@property (nonatomic, strong) UIFont *defaultFont;
@property (nonatomic, strong) UIFont *highlightedFont;

@property (nonatomic, strong) UIColor *controlBackgroundColor;
@property (nonatomic, strong) UIColor *buttonBackgroundColor;
@property (nonatomic, readonly, assign) NSInteger selectedIndex;

- (instancetype)initWithFrame:(CGRect)frame items:(NSArray *)items;

- (void)scrollToItemIndex:(NSInteger)itemIndex;
- (void)scrollToItemIndex:(NSInteger)itemIndex animated:(BOOL)animated;

@end
