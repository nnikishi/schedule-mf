//
//  ShareActivity.h
//  TV2 Generic Sportcenter
//
//  Created by Nickolai Nikishin on 5/12/14.
//  Copyright (c) 2014 Releaze A/S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareURLActivity : UIActivity

@property (nonatomic, strong) NSURL *URLToOpen;
@property (nonatomic, strong) NSString *schemePrefix;

@end
