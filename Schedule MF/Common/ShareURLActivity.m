//
//  ShareActivity.m
//  TV2 Generic Sportcenter
//
//  Created by Nickolai Nikishin on 5/12/14.
//  Copyright (c) 2014 Releaze A/S. All rights reserved.
//

#import "ShareURLActivity.h"

@implementation ShareURLActivity

- (NSString *)activityType {
	return NSStringFromClass([self class]);
}

- (UIImage *)activityImage {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return [UIImage imageNamed:[NSString stringWithFormat:@"%@%@%@", @"TV2GS_", self.activityType, @"-iPad"]];
    else
        return [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", @"TV2GS_", self.activityType]];
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
	for (id activityItem in activityItems) {
		if ([activityItem isKindOfClass:[NSURL class]]) {
			self.URLToOpen = activityItem;
		}
	}
}

@end
