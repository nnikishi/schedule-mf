//
//  NavigationBar.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/21/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationBar : UINavigationBar

+ (void)applyDesignForNavigationBar:(UINavigationBar *)navigationBar;

@end
