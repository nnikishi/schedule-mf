//
//  DataManager.h
//  UEFA Fantasy
//
//  Created by Nickolai Nikishin on 1/15/14.
//  Copyright (c) 2014 Releaze. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DataManager : NSObject

@property (nonatomic, readonly, strong) NSManagedObjectModel *objectModel;
@property (nonatomic, readonly, strong) NSManagedObjectContext *mainObjectContext;
@property (nonatomic, readonly, strong) NSManagedObjectContext *privateObjectContext;
@property (nonatomic, readonly, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (DataManager*)sharedManager;
+ (NSNumber *)numberFromBOOLString:(NSString *)string;
+ (NSNumber *)numberFromIntegerString:(NSString *)string;
+ (NSNumber *)numberFromDoubleString:(NSString *)string;
+ (NSDate *)dateFromString:(NSString *)string;
+ (NSDate *)dateFromTwitterString:(NSString *)string;
+ (NSString *)stringFromDate:(NSDate *)date;
+ (NSString *)humanStringFromDate:(NSDate *)date;
+ (NSString *)stringByRemovingTimeComponentInDateString:(NSString *)stringdate;
+ (NSDate *)dateWithoutTime:(NSDate *)datDate;
+ (NSString *)humanStringFromDateWithoutTime:(NSDate *)date;
+ (NSString *)humanStringFromTime:(NSDate *)date;
+ (NSString *)humanStringFromTwitterDate:(NSDate *)date;
+ (NSArray *)fetchDataStore:(NSManagedObjectContext *)context entity:(NSString *)entity predicate:(NSPredicate *)predicate;
+ (NSArray *)fetchDataStore:(NSManagedObjectContext *)context entity:(NSString *)entity predicate:(NSPredicate *)predicate sortDecriptorsArray:(NSArray *)sortDescriptorsArray;
+ (NSArray *)fetchDistinctValues:(NSString *)property context:(NSManagedObjectContext *) context entity:(NSString *)entity predicate:(NSPredicate *)predicate sortDecriptorsArray:(NSArray *)sortDescriptorsArray;

+ (NSString *)valueInString:(NSString *)string forKey:(NSString *)key;

@end
