//
//  TableObject.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/15/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "TableObject.h"

#define CONST_SEPARATOR @"~"
#define CONST_DIC_SEPARATOR @";"

@interface TableRow()

- (instancetype)initWithSettings:(NSDictionary *)settings;


@end

@implementation TableObject

+ (NSDictionary *)dictionaryFromStringSettings:(NSString *)stringSettings
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    NSArray *settingsArray = [stringSettings componentsSeparatedByString:CONST_DIC_SEPARATOR];
    
    for (NSString *property in settingsArray) {
        NSArray *propArray = [property componentsSeparatedByString:@"=="];
        if ([propArray count]!=2) {
            
            DLog(@"WRONG PROPERTY");
        } else {
            NSString *key = [propArray firstObject];
            NSString *val = [propArray lastObject];
            
            if (key && val) {
                [dictionary setObject:val forKey:key];
            }
        }
        
    }
    
    return dictionary;
}

- (instancetype)initWithSettings:(NSDictionary *)settings tableName:(NSString *)tableName
{
    self = [super init];
    
    if (self) {
        NSString *sections = [settings objectForKey:[NSString stringWithFormat:@"table==%@", tableName]];
        if (sections) {
            NSArray *sectionsArray = [sections componentsSeparatedByString:CONST_SEPARATOR];
            
            NSMutableArray *sectionsMutableArray = [[NSMutableArray alloc] initWithCapacity:[sectionsArray count]];
            
            for (NSString *sectionStr in sectionsArray) {
                
                TableSection *section = [[TableSection alloc] initWithSettings:settings sectionName:sectionStr];
                [sectionsMutableArray addObject:section];
                
            }
            
            self.tableSections = sectionsMutableArray;
            
        }
    }

   
    
    return self;
}

- (TableRow *)tableRowForIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.tableSections objectAtIndex:indexPath.section];
    TableRow *tableRow = [tableSection.tableRows objectAtIndex:indexPath.row];
    
    return tableRow;
}

@end

@implementation TableSection

- (instancetype)initWithSettings:(NSDictionary *)settings sectionName:(NSString *)sectionName
{
    self = [super init];
    
    
    
    
    NSString *components = [settings objectForKey:[NSString stringWithFormat:@"section==%@", sectionName]];
    NSDictionary *dic = [TableObject dictionaryFromStringSettings:components];
    
    self.title = [dic objectForKey:@"title"];
    
    NSString *rows = [dic objectForKey:@"rows"];
    if (rows) {
        NSArray *rowsArray = [rows componentsSeparatedByString:CONST_SEPARATOR];
        
        NSMutableArray *rowsMutableArray = [[NSMutableArray alloc] initWithCapacity:[rowsArray count]];
        
        for (NSString *rowStr in rowsArray) {
            
            TableRow *row = [TableRow tableRowWithSettings:settings rowName:rowStr];
            [rowsMutableArray addObject:row];
            
        }
        
        self.tableRows = rowsMutableArray;
        
    }

    
    
    return self;
}

@end

@implementation TableRow

+ (TableRow *)tableRowWithSettings:(NSDictionary *)settings rowName:(NSString *)rowName

{
    TableRow *tableRow = nil;
    NSString *components = [settings objectForKey:[NSString stringWithFormat:@"row==%@", rowName]];
    NSDictionary *dic = [TableObject dictionaryFromStringSettings:components];
   
    NSString *action  =[dic objectForKey:@"action"];
    
    if ([action isEqualToString:@"OpenController"]) {
        tableRow = [[TableRowOpenController alloc] initWithSettings:dic];
    } else if ([action isEqualToString:@"OpenURL"]) {
        tableRow = [[TableRowOpenURL alloc] initWithSettings:dic];
    } else if ([action isEqualToString:@"OpenFacebookURL"]) {
        tableRow = [[TableRowOpenFacebookURL alloc] initWithSettings:dic];
    } else if ([action isEqualToString:@"DialNumber"]) {
        tableRow = [[TableRowDialNumber alloc] initWithSettings:dic];
    } else if ([action isEqualToString:@"SendMail"]) {
        tableRow = [[TableRowSendMail alloc] initWithSettings:dic];
    }  else if ([action isEqualToString:@"WorkingTime"]) {
        tableRow = [[TableRowWorkingTime alloc] initWithSettings:dic];
    }
    
    
    
    
    return tableRow;
}

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    self = [super init];
    
    if (self) {
        self.title = [settings objectForKey:@"title"];
        self.preserveHighline = [[settings objectForKey:@"preserveHighlight"] boolValue];
    }
    
    return self;
}

@end

@implementation TableRowOpenController

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    self = [super initWithSettings:settings];
    
    if (self) {
        self.controller = [settings objectForKey:@"controller"];
        self.parameter = [settings objectForKey:@"parameter"];
    }
    
    return self;
}

@end

@implementation TableRowWorkingTime

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    self = [super initWithSettings:settings];
    
    if (self) {
        self.workingTimeArray = [[settings objectForKey:@"workingTime"] componentsSeparatedByString:CONST_SEPARATOR];
        self.fontColorStr = [settings objectForKey:@"fontColor"];
    }
    
    return self;
}

@end

@implementation TableRowOpenURL

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    self = [super initWithSettings:settings];
    
    if (self) {
        self.urlStr = [settings objectForKey:@"url"];
        self.openInternally = [[settings objectForKey:@"internal"] boolValue];
    }
    
    return self;
}

@end

@implementation TableRowOpenFacebookURL

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    self = [super initWithSettings:settings];
    
    if (self) {
        self.urlArray = [[settings objectForKey:@"url"] componentsSeparatedByString:CONST_SEPARATOR];
    }
    
    return self;
}

@end

@implementation TableRowDialNumber

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    self = [super initWithSettings:settings];
    
    if (self) {
        self.numbersToDial = [[settings objectForKey:@"phones"] componentsSeparatedByString:CONST_SEPARATOR];
    }
    
    return self;
}

@end

@implementation TableRowSendMail

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    self = [super initWithSettings:settings];
    
    if (self) {
        self.email = [settings objectForKey:@"email"];
    }
    
    return self;
}


@end
