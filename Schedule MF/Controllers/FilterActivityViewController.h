//
//  FilterActivityViewController.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/21/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "PortraitViewController.h"

@interface FilterActivityViewController : PortraitViewController

@property (nonatomic, strong) NSDictionary *dataDictionary;

@end
