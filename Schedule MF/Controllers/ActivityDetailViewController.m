//
//  ActivityDetailViewController.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/28/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "ActivityDetailViewController.h"
#import "DataManager.h"
#import "MFActivityCell.h"
#import "MFActivity+Methods.h"
#import "ShareURLActivity.h"
#import "SlideNavigationController.h"
#import "Flurry.h"

@interface ActivityDetailViewController () <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, UIAlertViewDelegate, MFActivityCellDelegate>

//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) MFActivity *activityForAlertView;
@property (nonatomic, strong) NSMutableDictionary *expandedCells;
@property (nonatomic, strong) NSMutableSet *expandedActivities;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@end

@implementation ActivityDetailViewController

#pragma mark Getters

- (NSMutableDictionary *)expandedCells
{
    if (!_expandedCells) {
        _expandedCells = [[NSMutableDictionary alloc] init];
    }
    
    return _expandedCells;
}

- (NSMutableSet *)expandedActivities
{
    if (!_expandedActivities) {
        _expandedActivities = [[NSMutableSet alloc] init];
    }
    
    return _expandedActivities;
}

- (NSPredicate *)dayPredicate
{
    if (self.weekDayForThisViewController) {
        
        NSInteger day = [[AppSettings sharedSettings].weekdaysArray indexOfObject:self.weekDayForThisViewController];
        
        day++;
        
        if (day!=NSNotFound) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"weekDay=%@", [NSNumber numberWithInteger:day]];
            
            return predicate;
        }
    }
    
    return nil;
}

- (NSPredicate *)compoundPredicate
{
    NSMutableArray *predicatesArray = [[NSMutableArray alloc] init];
    NSPredicate *dayPredicate = [self dayPredicate];
    
    if (self.menuPredicate) {
        [predicatesArray addObject:self.menuPredicate];
    }
    
    if (self.filterPredicate) {
        [predicatesArray addObject:self.filterPredicate];
    }
    
    if (dayPredicate) {
        [predicatesArray addObject:dayPredicate];
    }
    
    
    if ([predicatesArray count]>0) {
         NSCompoundPredicate *compPreciate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:predicatesArray];
        return compPreciate;
    }
    
    return nil;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (!_fetchedResultsController) {
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription
                                       entityForName:@"MFActivity" inManagedObjectContext:self.managedObjectContext];
        fetchRequest.entity = entity;
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc]
                                             initWithKey:@"weekDay" ascending:YES];
        NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc]
                                             initWithKey:@"startTime" ascending:YES];
        
      
//        NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc]
//                                             initWithKey:@"home" ascending:YES];
        
        
        
        NSArray *sortdecriptors = @[sortDescriptor1, sortDescriptor2];//, sortDescriptor3];
        [fetchRequest setSortDescriptors:sortdecriptors];
        
        [fetchRequest setFetchBatchSize:20];
        
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"periodId = %@", [self.fixtureDic objectForKey:@"id"]];
//        
       [fetchRequest setPredicate:[self compoundPredicate]];
        
        NSString *sectionKeyPath = nil;
        
        if ([self isFavorite]) {
            sectionKeyPath = @"weekDay";
        }
        
        _fetchedResultsController =
        [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                            managedObjectContext:self.managedObjectContext sectionNameKeyPath:sectionKeyPath
                                                       cacheName:nil];
        
        _fetchedResultsController.delegate = self;
    }
    return _fetchedResultsController;
    
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        _managedObjectContext = [DataManager sharedManager].mainObjectContext;
    }
    
    return _managedObjectContext;
}

#pragma mark Setters
- (void)setParameter:(NSString *)parameter
{
    _parameter = parameter;
    
    if ([_parameter length]>0) {
        self.menuPredicate = [NSPredicate predicateWithFormat: _parameter];
    } else {
        self.menuPredicate = nil;
    }
    
}

- (void)setNavBarTitle:(NSString *)navBarTitle
{
    _navBarTitle = navBarTitle;
}


#pragma mark UIView LifeCycle
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _fetchedResultsController.delegate = nil;
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)] && ![self isFavorite]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    
    [self performFetch];
    self.title = self.weekDayForThisViewController;

    NSInteger today = [AppSettings currentDay];
    NSInteger day = [[AppSettings sharedSettings].weekdaysArray indexOfObject:self.weekDayForThisViewController];
    
    if (today == day) {
        [self performSelector:@selector(scrollToCurrentItems) withObject:nil afterDelay:0.5];

    }
    
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processManagedObjectDidSaveNotification:) name:NOTIFICATION_DID_UPDATE_FAVORITES object:nil];
    
    self.tableView.sectionHeaderHeight = 1.0;
    self.tableView.sectionFooterHeight = 1.0;
    [self updateNavigationTitle];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)updateNavigationTitle
{
    
    self.navigationItem.title = self.navBarTitle;
    
    if ([self isFavorite]) {
        self.noDataLabel.text  = @"Записи в моем расписании отсутствуют. Их можно добавить из основного расписания";
    }
}

- (BOOL)isFavorite

{
    BOOL isFavorite = NO;
    
    if ([[self.menuPredicate predicateFormat] length] > 0 && [[self.menuPredicate predicateFormat] rangeOfString:@"isFavorite"].location!=NSNotFound) {
        isFavorite = YES;
    }
    
    return isFavorite;
}

- (void)processManagedObjectDidSaveNotification:(NSNotification *)notification
{
    id obj = notification.object;
    
    if (obj!=self && [self isFavorite]) {
        [self resetFetchedResultsControllerAndPerformFetch];
    }
   
}

#pragma mark UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MFActivityCell *cell = (MFActivityCell *)[tableView dequeueReusableCellWithIdentifier:
                                        NSStringFromClass([MFActivityCell class])];
    cell.delegate = self;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    MFActivityCell *theCell = (MFActivityCell *)cell;
    MFActivity *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    theCell.activityNameLabel.text = activity.name;
    theCell.activityNameLabel.textColor = [AppSettings colorFromHexString:activity.categoryColor];
    
    NSInteger hour = activity.startTime.integerValue / 60;
    NSInteger min = activity.startTime.integerValue - hour * 60;

    theCell.activityTimeHourLabel.text = [NSString stringWithFormat:@"%li", (long)hour];
    NSString *minStr = [NSString stringWithFormat:@"%li", (long)min];
    if ([minStr length]==1) {
        minStr = [NSString stringWithFormat:@"0%@", minStr];
    }
    theCell.activiyTimeMinuteLabel.text = minStr;

    theCell.activityPlaceLabel.text = activity.place;
    theCell.activityDurationLabel.text = [NSString stringWithFormat:@"%li мин", (long)[activity.duration integerValue]];
    
    theCell.commentsLabel.text = activity.comments;
    
    
//    if ([self isFavorite]) {
//        theCell.favoritesButton.hidden = YES;
//    } else {
//        theCell.favoritesButton.hidden = NO;
        if ([[activity isFavorite] boolValue]) {
            [theCell.favoritesButton setImage:[UIImage imageNamed:@"isFavoriteOn"] forState:UIControlStateNormal];
        } else {
             [theCell.favoritesButton setImage:[UIImage imageNamed:@"isFavoriteOff"] forState:UIControlStateNormal];
        }
        

  //  }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[self.fetchedResultsController sections] count];
    
    if ([self.fetchedResultsController.fetchedObjects count]>0) {
            self.noInfoView.hidden = YES;
    } else {
            self.noInfoView.hidden = NO;
    }

    
    
    return count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    NSArray *sections = [self.fetchedResultsController sections];
    if(sections.count)
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    
    //    if (numberOfRows==0) {
    //        self.tableView.hidden = YES;
    //        self.noInfoLabel.hidden = NO;
    //    } else {
    //        self.tableView.hidden = NO;
    //        _noInfoLabel.hidden = YES;
    //    }
    
    return numberOfRows;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if ([self isFavorite]) {
        return 30.0;
    }
    
    return 1.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
   
    return 1.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([self isFavorite]) {

        NSString *dayStr = [[[self.fetchedResultsController sections] objectAtIndex:section] name];
        NSInteger dayNum = [dayStr integerValue] - 1;
        NSString *day = [[AppSettings sharedSettings].weekdaysArray objectAtIndex:dayNum];
        return day;
        
    
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self heightForRowAtIndexPath:indexPath];
}

- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellHeight = 0;
    
    MFActivity *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([self.expandedActivities containsObject:[activity objectID]]) {
        NSIndexPath *indexPathNew = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        
        MFActivityCell *theCell = [self.expandedCells objectForKey:indexPathNew];
        
        if (theCell) {
            cellHeight = [theCell cellHeightExpanded];
        } else {
            cellHeight = [MFActivityCell cellHeight];
        }
    } else {
        cellHeight = [MFActivityCell cellHeight];

    }
	
    
	
	
	return cellHeight;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MFActivityCell *cell = (MFActivityCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    [self expandButtonDidTapped:cell];
    
    }

#pragma mark NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}


#pragma mark Fetch
- (void)performFetch
{
    NSError *error;
	if (![self.fetchedResultsController performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
	}
    
    
    [self.tableView reloadData];
    
//    NSUInteger itemsCount = [self.fetchedResultsController.fetchedObjects count];
//    
//    if (itemsCount==0) {
//        self.tableView.hidden = YES;
//        self.noInfoLabel.hidden = NO;
//    } else {
//        self.tableView.hidden = NO;
//        _noInfoLabel.hidden = YES;
//        [self.tableView reloadData];
//        
//    }
    
    
}

- (void)resetFetchedResultsControllerAndPerformFetch
{
    self.managedObjectContext = nil;
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
    [self performFetch];
    
    
}

- (void)scrollToCurrentItems
{
        
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm";
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    NSNumber *nowNumber  = [MFActivity doubleNumberFromTimeString:[dateFormatter stringFromDate:now]];
    
    MFActivity *nextActivity = nil;
    
    for (MFActivity *activity in self.fetchedResultsController.fetchedObjects) {
        if ([activity.startTime doubleValue]>= [nowNumber doubleValue]) {
            
            nextActivity = activity;
            break;
        }
    }
    
    if (nextActivity) {
        NSIndexPath *nextActivityIndexPath = [self.fetchedResultsController indexPathForObject:nextActivity];
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:nextActivityIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }

    
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        self.activityForAlertView.isFavorite = [NSNumber numberWithBool:NO];
        NSError *error = nil;
        [self.managedObjectContext save:&error];
        
        if (error) {
            DLog(@"error saving core data favorites %@", [error localizedDescription]);
        }
        
        [self logFavoritesActivityToFlurry:self.activityForAlertView];

    }
    
    
    
    self.activityForAlertView = nil;
}

#pragma mark MFActivityCellDelegate

- (void)shareButtonDidTapped:(UITableViewCell *)cell
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    MFActivity *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *day = [[AppSettings sharedSettings].weekdaysArray objectAtIndex:[activity.weekDay integerValue]-1];
    NSInteger hour = activity.startTime.integerValue / 60;
    NSInteger min = activity.startTime.integerValue - hour * 60;
    
    NSString *timeStr = [NSString stringWithFormat:@"%li", (long)hour];
    NSString *minStr = [NSString stringWithFormat:@"%li", (long)min];
    if ([minStr length]==1) {
        minStr = [NSString stringWithFormat:@"0%@", minStr];
    }
    NSString *text = [NSString stringWithFormat:@"%@ %@:%@ я иду на %@ в \"Мир Фитнеса\"!", day, timeStr, minStr, activity.name];
    
    
    NSURL *url = [NSURL URLWithString:@"http://mf.by"];
    
    NSMutableArray *activityItems = [[NSMutableArray alloc] initWithObjects:text, nil];
    
    if (url) {
        [activityItems addObject:url];
    }
    
   
    
//    NSArray *activities = @[[[ShareURLActivityChrome alloc] init],
//                            [[ShareURLActivitySafari alloc] init],
//                            //                            UIActivityTypeMail,
//                            //                            UIActivityTypeMessage,
//                            //                            UIActivityTypePostToFacebook,
//                            //                            UIActivityTypePostToTwitter,
//                            //                            UIActivityTypeCopyToPasteboard
//                            
//                            ];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc]
                                                    initWithActivityItems:activityItems
                                                    
                                                    applicationActivities:nil];
    
        activityController.excludedActivityTypes = [[NSArray alloc] initWithObjects:
                                                    UIActivityTypePostToWeibo,
                                                    UIActivityTypeSaveToCameraRoll,
                                                    UIActivityTypeAssignToContact,
                                                    UIActivityTypeAddToReadingList,
                                                    UIActivityTypePrint,
                                                    UIActivityTypePostToFlickr,
                                                    UIActivityTypePostToVimeo,
                                                    UIActivityTypePostToTencentWeibo,
                                                    UIActivityTypeAirDrop,
                                                    nil];
        
    
    [activityController setCompletionHandler:^(NSString *activityType, BOOL completed) {
        
        
    }];
    
    //    TV2GSNavigationController *navCon = [[TV2GSNavigationController alloc]
    //                                         initWithRootViewController:activityController];
    
    [self presentViewController:activityController
                       animated:YES completion:^{
                           
                           
                       }];

    
    
}
- (void)favoritesButtonDidTapped:(UITableViewCell *)cell
{
 
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    MFActivity *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([self isFavorite]) {
        
        if ([activity.isFavorite boolValue]) {
            self.activityForAlertView = activity;
            NSString *messageAlert  = [NSString stringWithFormat:@"Вы действительно хотите удалить запись %@ из Вашего расписания?", activity.name];
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Внимание" message:messageAlert delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil];
            [av show];
            
            return;
        }
    }
    

    activity.isFavorite = [NSNumber numberWithBool:![activity.isFavorite boolValue]];
    NSError *error = nil;
    [self.managedObjectContext save:&error];
    
    if (error) {
        DLog(@"error saving core data favorites %@", [error localizedDescription]);
    }
    
    [self logFavoritesActivityToFlurry:activity];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DID_UPDATE_FAVORITES object:self userInfo:nil];
}



- (void)expandButtonDidTapped:(UITableViewCell *)cell
{
    
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    MFActivity *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    BOOL expandedCell = YES;
	
	if ([self.expandedActivities containsObject:[activity objectID]]) {
		[self.expandedActivities removeObject:[activity objectID]];
        if ([self.expandedCells objectForKey:indexPath]) {
            [self.expandedCells removeObjectForKey:indexPath];

        }
        expandedCell = NO;
	} else {
		[self.expandedActivities addObject:[activity objectID]];
        [self.expandedCells setObject:cell forKey:indexPath];
        
	}
    
    MFActivityCell *theCell = (MFActivityCell *)cell;
    theCell.expanded = expandedCell;
	
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
    
	if (expandedCell) {
		
		NSInteger lastRow = 0;
		NSArray *indexesOfVisibleRows = [self.tableView indexPathsForVisibleRows];
		
		for (NSIndexPath *ip in indexesOfVisibleRows) {
			lastRow = MAX(lastRow, ip.row);
		}
		
		if (lastRow == indexPath.row) {
			[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
		}
		
		
	}

}

#pragma mark SlideNavigationControllerDelegate
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

#pragma mark Flurry

- (void)logFavoritesActivityToFlurry:(MFActivity *)activity
{
    
    NSString *flurryEvent = @"favorites added";
    
    if (![activity.isFavorite boolValue]) {
        flurryEvent = @"favorites removed";

    }

    [Flurry logEvent:flurryEvent
      withParameters:@{@"activityName" : activity.name,
                       @"activityType" :activity.type,
                       @"activityName" : activity.udid,
                       @"activityDay" : activity.weekDay,
                       @"activityStartTime" : activity.startTime}];

}




@end
