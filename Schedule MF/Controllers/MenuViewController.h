//
//  MenuViewController.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/28/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "PortraitViewController.h"



//@protocol MenuViewControllerDelegate <NSObject>
//
//@optional
//- (void)menuItemDidTappedAtIndexPath:(NSIndexPath *)indexPath;
//- (void)menuItemDidTappedAtIndexPath:(NSIndexPath *)indexPath predicate:(NSPredicate *)predicate;
//
//
//@end


@interface MenuViewController : PortraitViewController

//@property (nonatomic, weak) id <MenuViewControllerDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *selectedRow;


- (void)didSelectMenuItemAtIndexPath:(NSIndexPath *)indexPath;

@end
