//
//  WorkingTimeViewController.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/20/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "WorkingTimeViewController.h"
#import "SlideNavigationController.h"
#import "TableObject.h"
#import "WorkingTimeCell.h"

@interface WorkingTimeViewController () <SlideNavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) TableObject *workingTimeTableObject;
@end

@implementation WorkingTimeViewController

#pragma mark Getters
- (TableObject *)workingTimeTableObject
{
    if (!_workingTimeTableObject) {
        _workingTimeTableObject = [AppSettings sharedSettings].workingTimeTable;
    }
    
    return _workingTimeTableObject;
}

#pragma mark Setters
- (void)setParameter:(NSString *)parameter
{
    _parameter = parameter;
    _navBarTitle = parameter;
}

- (void)setNavBarTitle:(NSString *)navBarTitle
{
    _navBarTitle = navBarTitle;
    [self updateNavigationTitle];
}

#pragma mark UIView LifeCycle
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.sectionHeaderHeight = 1.0;
    self.tableView.sectionFooterHeight = 1.0;
   
    [self registerForNotificationCenterEvents];
    [self updateNavigationTitle];
}

- (void)updateNavigationTitle
{
    self.navigationItem.title = self.navBarTitle;
    
}

- (void)registerForNotificationCenterEvents
{
    __weak WorkingTimeViewController *wself = self;
    
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NOTIFICATION_DID_RECEIVE_SETTINGS object:nil queue:mainQueue usingBlock:^(NSNotification *note) {
        [wself.tableView reloadData];
        
    }];
    
    
}

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WorkingTimeCell *cell = (WorkingTimeCell *)[tableView dequeueReusableCellWithIdentifier:
                                  NSStringFromClass([WorkingTimeCell class])];
    
    TableRowWorkingTime *row = (TableRowWorkingTime *)[self.workingTimeTableObject tableRowForIndexPath:indexPath];

    cell.titleLabel.text = row.title;
    cell.workingTimeLabel.text  =[row.workingTimeArray componentsJoinedByString:@"  "];
    
    if ([row.fontColorStr length]>0) {
        cell.titleLabel.textColor = [AppSettings colorFromHexString:row.fontColorStr];
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger sectionsCount = [self.workingTimeTableObject.tableSections count];
    return sectionsCount;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TableSection *tableSection = [self.workingTimeTableObject.tableSections objectAtIndex:section];
    NSInteger numberOfRows = [tableSection.tableRows count];
    return numberOfRows;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TableSection *tableSection = [self.workingTimeTableObject.tableSections objectAtIndex:section];
    NSString *text = tableSection.title;
    return text;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
   return 1.0;
}


#pragma mark SlideNavigationControllerDelegate
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

@end
