//
//  FilterActivityViewController.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/21/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "FilterActivityViewController.h"
#import "FilterActivityCell.h"

@interface FilterActivityViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableOrderedSet *selectedActivities;
@property (nonatomic, strong) NSArray *sortedSectionsArray;

@property (nonatomic, strong) UIBarButtonItem *clearFilterBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *closeBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *applyBarButtonItem;


@end

@implementation FilterActivityViewController

#pragma mark Getters

- (UIBarButtonItem *)clearFilterBarButtonItem
{
    if (!_clearFilterBarButtonItem) {
        _clearFilterBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Очистить фильтр" style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter:)];
    }
    
    return _clearFilterBarButtonItem;
    
}

- (UIBarButtonItem *)closeBarButtonItem
{
    if (!_closeBarButtonItem) {
        _closeBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Закрыть" style:UIBarButtonItemStylePlain target:self action:@selector(close:)];
    }
    
    return _closeBarButtonItem;
    
}

- (UIBarButtonItem *)applyBarButtonItem
{
    if (!_applyBarButtonItem) {
        _applyBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Применить" style:UIBarButtonItemStylePlain target:self action:@selector(applyFilter:)];
    }
    
    return _applyBarButtonItem;
    
}
- (NSMutableOrderedSet *)selectedActivities
{
    if (!_selectedActivities) {
        _selectedActivities = [[NSMutableOrderedSet alloc] init];
    }
    return _selectedActivities;
}

- (NSArray *)sortedSectionsArray
{
    if (!_sortedSectionsArray) {
        _sortedSectionsArray = [[self.dataDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    
    return _sortedSectionsArray;
}

- (void)updateNavigationBar
{
    self.navigationItem.leftBarButtonItem = self.closeBarButtonItem;
    
    if ([_selectedActivities count]) {
        self.navigationItem.rightBarButtonItem = self.clearFilterBarButtonItem;
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

#pragma mark Setters
- (void)setDataDictionary:(NSDictionary *)dataDictionary
{
    _dataDictionary = dataDictionary;
    
    if (_sortedSectionsArray) {
        _sortedSectionsArray = nil;
        [_tableView reloadData];
    } else {
        _sortedSectionsArray = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.sectionHeaderHeight = 1.0;
    self.tableView.sectionFooterHeight = 1.0;
    [self updateNavigationBar];
}


- (NSString *)sectionNameForIndexPath:(NSIndexPath *)indexPath
{
    return [[self sortedSectionsArray] objectAtIndex:indexPath.section];
}



- (NSString *)activityNameForIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionName = [self sectionNameForIndexPath:indexPath];
    NSArray *rows = [self.dataDictionary objectForKey:sectionName];
    return [rows objectAtIndex:indexPath.row];
}

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterActivityCell *cell = (FilterActivityCell *)[tableView dequeueReusableCellWithIdentifier:
                                                NSStringFromClass([FilterActivityCell class])];
    
   

    NSString *activityName = [self activityNameForIndexPath:indexPath];
    cell.activityNameLabel.text = activityName;
    
    if ([_selectedActivities containsObject:activityName]) {
        cell.selectedImage.highlighted = YES;
    } else {
        cell.selectedImage.highlighted = NO;

    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger sectionsCount = [self.sortedSectionsArray count];
    return sectionsCount;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *rows = [self.dataDictionary objectForKey:[self.sortedSectionsArray objectAtIndex:section]];
    NSInteger numberOfRows = [rows count];
    return numberOfRows;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *text = [self.sortedSectionsArray objectAtIndex:section];
    return text;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *activityName = [self activityNameForIndexPath:indexPath];
    
    if ([_selectedActivities containsObject:activityName]) {
        [_selectedActivities removeObject:activityName];
    } else {
        [self.selectedActivities addObject:activityName];
    }
    
 
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self updateNavigationBar];

}

#pragma mark NavigationItems actions

- (void)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)applyFilter:(id)sender
{
    [self close:nil];
}

- (void)clearFilter:(id)sender
{
    [_selectedActivities removeAllObjects];
    [_tableView reloadData];
}
@end
