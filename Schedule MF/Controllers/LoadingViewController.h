//
//  ViewController.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PortraitViewController.h"

@interface LoadingViewController: PortraitViewController

@end
