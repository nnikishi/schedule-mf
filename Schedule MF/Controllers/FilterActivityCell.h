//
//  FilterActivityCell.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/21/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterActivityCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *selectedImage;

@property (weak, nonatomic) IBOutlet UILabel *activityNameLabel;
@end
