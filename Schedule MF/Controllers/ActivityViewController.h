//
//  ActivityViewController.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/28/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "PortraitViewController.h"

@interface ActivityViewController : PortraitViewController

@property (nonatomic, strong) NSString *parameter;
@property (nonatomic, strong) NSString *navBarTitle;

@end
