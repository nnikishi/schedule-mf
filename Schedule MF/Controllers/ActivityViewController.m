//
//  ActivityViewController.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/28/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//


#import "ActivityViewController.h"
#import "SlideNavigationController.h"
#import "ActivityDetailViewController.h"
#import "ScrollableControl.h"
#import "PageViewController.h"
#import "FilterActivityViewController.h"
#import "DataManager.h"
#import "MFActivity.h"
#import "NavigationBar.h"

@interface ActivityViewController () <SlideNavigationControllerDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate, ScrollableControlDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *placeholderViewForScrollableSwitchers;
@property (weak, nonatomic) IBOutlet UIView *placeholderForPageViewController;

@property (strong, nonatomic) PageViewController *pageViewController;
@property (strong, nonatomic) NSOrderedSet *weekdaysSet;
@property (strong, nonatomic) NSMutableDictionary *controllersDic;
@property (strong, nonatomic) ScrollableControl *scrollableControl;
@property (nonatomic, strong) NSPredicate *menuPredicate;
@property (nonatomic, strong) NSPredicate *filterPredicate;

@property (nonatomic, strong) UIBarButtonItem *filterOffBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *filterOnBarButtonItem;

@property (nonatomic, strong) FilterActivityViewController *filterViewCon;


@end

@implementation ActivityViewController

#pragma mark Getters

- (FilterActivityViewController *)filterViewCon
{
    if (!_filterViewCon) {
        
        _filterViewCon = [[AppSettings mainStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([FilterActivityViewController class])];
        
        NSSortDescriptor *sortDescriptior1 = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
        NSSortDescriptor *sortDescriptior2 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];

        NSArray *fetchedItems = [DataManager fetchDataStore:[DataManager sharedManager].mainObjectContext entity:NSStringFromClass([MFActivity class]) predicate:self.menuPredicate sortDecriptorsArray:@[sortDescriptior1, sortDescriptior2]];
        
        NSMutableDictionary *objects = [[NSMutableDictionary alloc] init];
        
        for (MFActivity *activity in fetchedItems) {
            
            if (activity.category) {
                NSMutableOrderedSet *activityForCategory = [objects objectForKey:activity.category];
                if (activityForCategory) {
                    
                    if (![activityForCategory containsObject:activity.name]) {
                        NSMutableOrderedSet *newArray  = [[NSMutableOrderedSet alloc] initWithOrderedSet:activityForCategory];
                        [newArray addObject:activity.name];
                        [objects setObject:newArray forKey:activity.category];
                    }
                    
                   
                } else {
                    NSMutableOrderedSet *theSet = [[NSMutableOrderedSet alloc] initWithObject:activity.name];
                    [objects setObject:theSet forKey:activity.category];
                }

            }
            
        }
        
        NSMutableDictionary *finalDictionary = [[NSMutableDictionary alloc] init];
        
        for (NSString *key in [objects allKeys]) {
            NSOrderedSet *theSet = [objects objectForKey:key];
            [finalDictionary setObject:[theSet array] forKey:key];
        }
        
        _filterViewCon.dataDictionary = finalDictionary;
        
    }
    
    
    return _filterViewCon;
}

- (UIBarButtonItem *)filterOffBarButtonItem
{
    
    if (!_filterOffBarButtonItem) {
        _filterOffBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navbar_filterOff"] style:UIBarButtonItemStylePlain target:self action:@selector(filterBarButtonTapped:)];
    }
    
    return _filterOffBarButtonItem;
}

- (UIBarButtonItem *)filterOnBarButtonItem
{
    
    if (!_filterOnBarButtonItem) {
        UIImage *image = [[UIImage imageNamed:@"navbar_filterOn"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

        _filterOnBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(filterBarButtonTapped:)];
    }
    
    return _filterOnBarButtonItem;
}

- (ScrollableControl *)scrollableControl
{
    if (!_scrollableControl) {
        _scrollableControl = [[ScrollableControl alloc] initWithFrame:self.placeholderViewForScrollableSwitchers.frame items:[AppSettings sharedSettings].weekdaysArray];
        _scrollableControl.highlightedViewColor = [UIColor yellowColor];
        _scrollableControl.buttonBackgroundColor = [AppSettings sharedSettings].navBarColor;
        _scrollableControl.controlBackgroundColor = [AppSettings sharedSettings].navBarColor;
        _scrollableControl.scrollableControlDelegate = self;
    }
    
    return _scrollableControl;
}

- (NSMutableDictionary *)controllersDic
{
    if (!_controllersDic) {
        _controllersDic = [[NSMutableDictionary alloc] init];
    }
    
    return _controllersDic;
}


- (NSOrderedSet *)weekdaysSet
{
    if (!_weekdaysSet) {
        _weekdaysSet = [NSOrderedSet orderedSetWithArray:[AppSettings sharedSettings].weekdaysArray];
    }
    
    return _weekdaysSet;
}

- (PageViewController *)pageViewController
{
    if (!_pageViewController) {
        _pageViewController = [[PageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        _pageViewController.view.frame = self.placeholderForPageViewController.bounds;
        DLog(@"frame %@", NSStringFromCGRect(self.placeholderForPageViewController.bounds));

        _pageViewController.dataSource = self;
        _pageViewController.delegate = self;

        _pageViewController.view.hidden = YES;
        
        [self.pageViewController willMoveToParentViewController:self];
        [self addChildViewController:self.pageViewController];
        [self.pageViewController didMoveToParentViewController:self];
     
        [self.placeholderForPageViewController addSubview:_pageViewController.view];

    }
    
    return _pageViewController;
}

#pragma mark
- (void)setParameter:(NSString *)parameter
{
    _parameter = parameter;
    
    if ([_parameter length]>0) {
        self.menuPredicate = [NSPredicate predicateWithFormat: _parameter];
    } else {
        self.menuPredicate = nil;
    }
    
}

- (void)setNavBarTitle:(NSString *)navBarTitle
{
    _navBarTitle = navBarTitle;
}

#pragma mark UIView LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateNavigationTitle];
    [self setupNavigationBar];
}

- (void)setupNavigationBar
{
    
    [NavigationBar applyDesignForNavigationBar:self.navigationController.navigationBar];
    
//    if (self.filterPredicate) {
//        self.navigationItem.rightBarButtonItem = self.filterOnBarButtonItem;
//    } else {
//        self.navigationItem.rightBarButtonItem = self.filterOffBarButtonItem;
//
//    }

}

- (void)updateNavigationTitle
{
    
    
    
    self.title = self.navBarTitle;
}

- (void)viewDidLayoutSubviews
{
    if (!_pageViewController) {
        
        
        [self performSelector:@selector(setInitialViewControllerForPageViewCon) withObject:nil afterDelay:0.2];
        
        
    }
    
   
    
    
    
}

- (void)setInitialViewControllerForPageViewCon
{
    
    NSInteger today = [AppSettings currentDay];
    
    
    ActivityDetailViewController *advc = [self activityDetailViewControllerForIndex:today];

    [self.pageViewController setViewControllers:@[advc] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        
        
        
        
    }];
    
    self.pageViewController.view.hidden = NO;
    
    if (!_scrollableControl) {
        [self.view addSubview:self.scrollableControl];
        [self.scrollableControl scrollToItemIndex:today];
        
    }

}


#pragma mark Methods
- (ActivityDetailViewController *)activityDetailViewControllerForIndex:(NSInteger)index
{
    NSNumber *key = [NSNumber numberWithInteger:index];
    ActivityDetailViewController *advc = [self.controllersDic objectForKey:key];
    
    if (!advc) {
        advc = [[AppSettings mainStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([ActivityDetailViewController class])];
        advc.weekDayForThisViewController = [self.weekdaysSet objectAtIndex:index];
        advc.menuPredicate = self.menuPredicate;
        [self.controllersDic setObject:advc forKey:key];
    }
    
    return advc;
}

- (void)filterBarButtonTapped:(id)sender

{
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:self.filterViewCon];
    
    [NavigationBar applyDesignForNavigationBar:navCon.navigationBar];

    
    [self.navigationController presentViewController:navCon animated:YES completion:^{
        
    }];
    
    if (sender==self.filterOffBarButtonItem) {
        self.navigationItem.rightBarButtonItem = self.filterOnBarButtonItem;
    } else {
        self.navigationItem.rightBarButtonItem = self.filterOffBarButtonItem;

    }
}

#pragma mark UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{

    ActivityDetailViewController *advc = (ActivityDetailViewController *)viewController;
    NSString *weekDay = advc.weekDayForThisViewController;
    
    NSInteger index = [self.weekdaysSet indexOfObject:weekDay];
    
    if (index>0) {
        return [self activityDetailViewControllerForIndex:index-1];
    } else {
        return [self activityDetailViewControllerForIndex:[self.weekdaysSet count]-1];

    }
    
    
    return nil;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
    
    ActivityDetailViewController *advc = (ActivityDetailViewController *)viewController;
    NSString *weekDay = advc.weekDayForThisViewController;
    
    NSInteger index = [self.weekdaysSet indexOfObject:weekDay];
    
    if (index<[self.weekdaysSet count]-1) {
        return [self activityDetailViewControllerForIndex:index+1];
    } else {
        return [self activityDetailViewControllerForIndex:0];

    }
    
    
    return nil;
    
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {

}

- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed {
    
    ActivityDetailViewController *advc = [pageViewController.viewControllers firstObject];

    NSString *weekDay = advc.weekDayForThisViewController;
    NSInteger index = [self.weekdaysSet indexOfObject:weekDay];
    
    if (completed) {
        [self.scrollableControl scrollToItemIndex:index animated:YES];
    }
   

}


#pragma mark SlideNavigationControllerDelegate
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

#pragma mark ScrollableControlDelegate
- (void)scrollableControl:(id)sender didTappedButtonAtIndex:(NSInteger)buttonIndex
{
    ActivityDetailViewController *currentAdvc = [self.pageViewController.viewControllers firstObject];
    NSString *weekDay = currentAdvc.weekDayForThisViewController;
    NSInteger index = [self.weekdaysSet indexOfObject:weekDay];
    

    if (index==buttonIndex) {
        return;
    }
    
    NSInteger direction = UIPageViewControllerNavigationDirectionForward;
    

    
    if (buttonIndex<index) {
        direction  =UIPageViewControllerNavigationDirectionReverse;
        
    }
    
    
    ActivityDetailViewController *advc = [self activityDetailViewControllerForIndex:buttonIndex];
    
    [self.pageViewController setViewControllers:@[advc] direction:direction invalidateCache:YES animated:YES completion:^(BOOL finished) {
        
    }];
}


//#pragma mark UIScrollViewDelegate
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//
//    if (scrollView.contentOffset.y > 60) {
//        [scrollView setContentOffset:CGPointMake(0, 60)];
//    }
//}
@end
