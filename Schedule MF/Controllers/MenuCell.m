//
//  MenuCell.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/1/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self updateUI];
}

- (void)setIsCellSelected:(BOOL)isCellSelected
{
    _isCellSelected = isCellSelected;
    [self updateUI];
}

- (void)updateUI
{
    self.selectedView.hidden = !self.isCellSelected;

}
- (IBAction)naviButtonDidTapped:(id)sender {
}

@end
