//
//  ActivityDetailViewController.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/28/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "PortraitViewController.h"

@interface ActivityDetailViewController : PortraitViewController

@property (nonatomic, strong) NSString *weekDayForThisViewController;
@property (nonatomic, strong) NSPredicate *categoryPredicate;
@property (nonatomic, strong) NSPredicate *filterPredicate;
@property (nonatomic, strong) NSPredicate *menuPredicate;
@property (weak, nonatomic) IBOutlet UIView *noInfoView;


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSString *parameter;
@property (nonatomic, strong) NSString *navBarTitle;


@end
