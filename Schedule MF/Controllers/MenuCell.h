//
//  MenuCell.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/1/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuItemLabel;
@property (weak, nonatomic) IBOutlet UIView *selectedView;
@property (nonatomic, assign) BOOL isCellSelected;
@property (weak, nonatomic) IBOutlet UIButton *naviButton;

@end
