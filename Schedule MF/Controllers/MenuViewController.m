//
//  MenuViewController.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/28/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCell.h"
#import "TableViewHeader.h"
#import "TableObject.h"
#import "SlideNavigationController.h"
#import <MessageUI/MessageUI.h>
#import "Flurry.h"

const CGFloat CONST_TABLEVIEWHEADER_HEIGHT = 18.0;

@interface MenuViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) TableObject *tableObject;
@property (nonatomic, strong) NSMutableDictionary *viewControllersDic;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) TableViewHeader *genericHeaderView;

@property (weak, nonatomic) IBOutlet UIView *statusBarOverlayView;


@end



@implementation MenuViewController
@synthesize selectedRow = _selectedRow;


#pragma Getters

- (NSMutableDictionary *)viewControllersDic
{
    if (!_viewControllersDic) {
        _viewControllersDic = [[NSMutableDictionary alloc] init];
    }
    
    return _viewControllersDic;
}


- (TableObject *)tableObject
{
    if (!_tableObject) {
        _tableObject = [AppSettings sharedSettings].menuTable;
    }
    
    return _tableObject;
}


- (NSIndexPath *)selectedRow
{
    if (!_selectedRow) {
        _selectedRow = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    
    return _selectedRow;
}


- (void)setSelectedRow:(NSIndexPath *)selectedRow
{
    
    if (!(selectedRow.row == _selectedRow.row && selectedRow.section == _selectedRow.section)) {
        
        if (_selectedRow) {
            MenuCell *previouslySelectedCell =  (MenuCell *)[self.tableView cellForRowAtIndexPath:_selectedRow];
            previouslySelectedCell.isCellSelected = NO;
        }
        
        MenuCell *newSelectedCell =  (MenuCell *)[self.tableView cellForRowAtIndexPath:selectedRow];
        newSelectedCell.isCellSelected = YES;
        
        _selectedRow = selectedRow;
    }
}

- (TableViewHeader *)genericHeaderView
{
    if (!_genericHeaderView) {
        _genericHeaderView = [[TableViewHeader alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), CONST_TABLEVIEWHEADER_HEIGHT)];
    }
    
    return _genericHeaderView;
}

#pragma mark UIView LifeCycle

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerForNotificationCenterEvents];
    self.statusBarOverlayView.backgroundColor = [AppSettings sharedSettings].navBarColor;
}

- (void)registerForNotificationCenterEvents
{
    __weak MenuViewController *wself = self;
    
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NOTIFICATION_DID_RECEIVE_SETTINGS object:nil queue:mainQueue usingBlock:^(NSNotification *note) {
        _viewControllersDic = nil;
        [wself.tableView reloadData];
    
    }];
    

}

#pragma mark UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:
                                              NSStringFromClass([MenuCell class])];
//    NSInteger groupExercisesPosition = [self groupExercisesPosition];
//    NSInteger childrenExercisesPosition = [self childrenExercisesPosition];
//    NSInteger aboutPosition = [self aboutPosition];
//    NSInteger aboutAppPosition = [self aboutAppPosition];
//    
//    NSArray *dataFeedArray = nil;
//    NSInteger section = indexPath.section;
//    
//    if (section==groupExercisesPosition) {
//        dataFeedArray = [AppSettings sharedSettings].groupExercisesArray;
//    } else
//        
//        if (section==childrenExercisesPosition) {
//            dataFeedArray = [AppSettings sharedSettings].childrenExercisesArray;
//        } else
//            
//            if (section==aboutPosition) {
//                dataFeedArray = [AppSettings sharedSettings].aboutRowsArray;
//            } else
//                
//                if (section==aboutAppPosition) {
//                    dataFeedArray = [AppSettings sharedSettings].aboutAppRowsArray ;
//                }
//    
//    
//    
//    cell.menuItemLabel.text = [dataFeedArray objectAtIndex:indexPath.row];
    
    TableRow *row = [self.tableObject tableRowForIndexPath:indexPath];
    
    cell.menuItemLabel.text = row.title;
    
    if (self.selectedRow.row == indexPath.row && self.selectedRow.section==indexPath.section) {
        cell.isCellSelected = YES;
    } else {
        cell.isCellSelected = NO;
    }
    
    if ([row isMemberOfClass:[TableRowOpenController class]]) {
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    } else if ([row isMemberOfClass:[TableRowOpenURL class]]) {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    } else if ([row isMemberOfClass:[TableRowDialNumber class]]) {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    } else if ([row isMemberOfClass:[TableRowSendMail class]]) {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger sectionsCount = [self.tableObject.tableSections count];
    
//    if ([self groupExercisesPosition]!=NSNotFound) {
//        sectionsCount++;
//    }
//    
//    if ([self childrenExercisesPosition]!=NSNotFound) {
//        sectionsCount++;
//    }
//    
//    if ([self aboutPosition]!=NSNotFound) {
//        sectionsCount++;
//    }
//    
//    if ([self aboutAppPosition]!=NSNotFound) {
//        sectionsCount++;
//    }
//    
    return sectionsCount;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
//    NSInteger groupExercisesPosition = [self groupExercisesPosition];
//    NSInteger childrenExercisesPosition = [self childrenExercisesPosition];
//    NSInteger aboutPosition = [self aboutPosition];
//    NSInteger aboutAppPosition = [self aboutAppPosition];
//
//    NSInteger numberOfRows = 0;
//    
//    if (section==groupExercisesPosition) {
//        numberOfRows = [[AppSettings sharedSettings].groupExercisesArray count];
//    } else
//    
//    if (section==childrenExercisesPosition) {
//        numberOfRows = [[AppSettings sharedSettings].childrenExercisesArray count];
//    } else
//    
//    if (section==aboutPosition) {
//        numberOfRows = [[AppSettings sharedSettings].aboutRowsArray count];
//    } else
//    
//    if (section==aboutAppPosition) {
//        numberOfRows = [[AppSettings sharedSettings].aboutAppRowsArray count];
//    }
    
    TableSection *tableSection = [self.tableObject.tableSections objectAtIndex:section];

    NSInteger numberOfRows = [tableSection.tableRows count];

    
    return numberOfRows;
}


 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *textForHeaderInSection = [self textForHeaderInSection:section];
    return textForHeaderInSection;

}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//    NSString *textForHeaderInSection = [self textForHeaderInSection:section];
//    
//    if (textForHeaderInSection) {
//        TableViewHeader *view = [self.genericHeaderView duplicate];
//        view.captionText = textForHeaderInSection;
//        return view;
//
//    }
//    
//    return nil;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    NSString *textForHeaderInSection = [self textForHeaderInSection:section];
//    
//    if (textForHeaderInSection) {
//        
//        return CGRectGetHeight(self.genericHeaderView.frame);
//        
//    }
//
//    return 0.0;
//}

#pragma mark Methods
- (UIViewController *)viewControllerForIndexPath:(NSIndexPath *)indexPath
                                 controllerClass:(NSString *)controllerClass
                                       parameter:(NSString *)parameter
{
    UIViewController *viewCon = [self.viewControllersDic objectForKey:indexPath];
    
    
    
    if (!viewCon) {
        
        Class viewContollerClass = NSClassFromString(controllerClass);
        id object = [[AppSettings mainStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([viewContollerClass class])];
        
        if (parameter) {
            [object setValue:parameter forKey:@"parameter"];
        }
    
        viewCon = object;
        
        [self.viewControllersDic setObject:viewCon forKey:indexPath];
        
    }
    
    return viewCon;
}

- (void)generateMailToAddress:(NSString *)address
{
    
    BOOL cannotSendMail = NO;
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {

        [UINavigationBar appearance].barTintColor = [AppSettings sharedSettings].navBarColor;
        [UINavigationBar appearance].tintColor = [UIColor whiteColor];
		
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.navigationBar.translucent = NO;
        
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
        NSString *applicationName = [[NSBundle mainBundle] localizedInfoDictionary][@"CFBundleDisplayName"];
        

        
		mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:[NSString stringWithFormat:@"приложение iOS %@ (%@)", applicationName, version]];
  		[mailViewController setToRecipients:@[address]];
        
        
		mailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        if([mailClass canSendMail]) {
            
            [[SlideNavigationController sharedInstance] presentViewController:mailViewController animated:YES completion:^{
                mailViewController.topViewController.navigationItem.title = @"";

            }];

        } else {
            cannotSendMail = YES;
            
            
        }
    } else {
        cannotSendMail = YES;
    }
}

#pragma mark Helper Methods
- (NSString *)textForHeaderInSection:(NSInteger)section
{
//    NSInteger groupExercisesPosition = [self groupExercisesPosition];
//    NSInteger childrenExercisesPosition = [self childrenExercisesPosition];
//    NSInteger aboutPosition = [self aboutPosition];
//    NSInteger aboutAppPosition = [self aboutAppPosition];
//    
//    NSString *text = nil;
//    
//    if (section==groupExercisesPosition) {
//        text = [AppSettings sharedSettings].groupExercisesHeader;
//    } else
//        
//        if (section==childrenExercisesPosition) {
//            text = [AppSettings sharedSettings].childrenExercisesHeader;
//        } else
//            
//            if (section==aboutPosition) {
//                text = [AppSettings sharedSettings].aboutHeader;
//            } else
//                
//                if (section==aboutAppPosition) {
//                    text = [AppSettings sharedSettings].aboutAppHeader;
//                }
//
    TableSection *tableSection = [self.tableObject.tableSections objectAtIndex:section];
    NSString *text = tableSection.title;

    return text;
}

//- (NSInteger)groupExercisesPosition
//{
//    NSInteger position = NSNotFound;
//    
//    if ([[AppSettings sharedSettings].groupExercisesArray count]>0) {
//        position = 0;
//    }
//    
//    return position;
//}
//
//- (NSInteger)childrenExercisesPosition
//{
//    NSInteger position = NSNotFound;
//    
//    
//    if ([[AppSettings sharedSettings].childrenExercisesArray count]>0) {
//        if ([self groupExercisesPosition]!=NSNotFound) {
//            position = 1;
//        } else {
//            position = 0;
//        }
//    }
//
//    
//    
//    return position;
//}
//
//- (NSInteger)aboutPosition
//{
//    NSInteger position = NSNotFound;
//    
//    if ([[AppSettings sharedSettings].aboutRowsArray count]>0) {
//        
//        position = 0;
//        
//        if ([self groupExercisesPosition]!=NSNotFound) {
//            position++;
//        }
//        
//        if ([self childrenExercisesPosition]!=NSNotFound) {
//            position ++;
//        }
//    }
//
//    return position;
//    
//}
//
//- (NSInteger)aboutAppPosition
//{
//    NSInteger position = NSNotFound;
//    
//    if ([[AppSettings sharedSettings].aboutAppRowsArray count]>0) {
//        
//        position = 0;
//        
//        if ([self groupExercisesPosition]!=NSNotFound) {
//            position++;
//        }
//        
//        if ([self childrenExercisesPosition]!=NSNotFound) {
//            position ++;
//        }
//        
//        if ([self aboutPosition]!=NSNotFound) {
//            position ++;
//        }
//    }
//    
//    return position;
//    
//}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self didSelectMenuItemAtIndexPath:indexPath];
}

#pragma mark delegate

- (void)didSelectMenuItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    TableRow *row = [self.tableObject tableRowForIndexPath:indexPath];

    if (row.preserveHighline) {
        self.selectedRow = indexPath;
    }
    
    if ([row isMemberOfClass:[TableRowOpenController class]]) {
        
       TableRowOpenController *theRow = (TableRowOpenController *)row;
        UIViewController *viewCon = [self viewControllerForIndexPath:indexPath controllerClass:theRow.controller parameter:theRow.parameter];
        
        [viewCon setValue:row.title forKey:@"navBarTitle"];
        
         [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:viewCon withSlideOutAnimation:YES andCompletion:NULL];
        
        [Flurry logEvent:NSStringFromClass([TableRowOpenController class]) withParameters:@{@"title" : row.title}];

        
    } else if ([row isMemberOfClass:[TableRowOpenURL class]]) {
        TableRowOpenURL *theRow = (TableRowOpenURL *)row;
        NSURL *theURL = [NSURL URLWithString:theRow.urlStr];
        
        if (theURL && [[UIApplication sharedApplication] canOpenURL:theURL]) {
            [[UIApplication sharedApplication] openURL:theURL];
            [Flurry logEvent:NSStringFromClass([TableRowOpenURL class])
              withParameters:@{@"url" : theRow.urlStr}];
        }
        
    } else if ([row isMemberOfClass:[TableRowOpenFacebookURL class]]) {
        TableRowOpenFacebookURL *theRow = (TableRowOpenFacebookURL *)row;
        
        for (NSString *urlStr in theRow.urlArray) {
            NSURL *theURL = [NSURL URLWithString:urlStr];
            
            if (theURL && [[UIApplication sharedApplication] canOpenURL:theURL]) {
                [[UIApplication sharedApplication] openURL:theURL];
                [Flurry logEvent:NSStringFromClass([TableRowOpenFacebookURL class])
                  withParameters:@{@"url" : [theURL absoluteString]}];
                break;
            }
        }
        
       
        
    } else if ([row isMemberOfClass:[TableRowDialNumber class]]) {
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
            
            TableRowDialNumber *theRow = (TableRowDialNumber *)row;
            
            if ([theRow.numbersToDial count]>0) {
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Позвонить в клуб" delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles: nil];
                
                for (NSString *title in theRow.numbersToDial) {
                    [actionSheet addButtonWithTitle:title];
                }
                
                [actionSheet showInView:self.view];
                
            }
        }
        
       

        
    } else if ([row isMemberOfClass:[TableRowSendMail class]]) {
        TableRowSendMail *theRow = (TableRowSendMail *)row;
        if ([theRow.email length]>0) {
            [self generateMailToAddress:theRow.email];
            [Flurry logEvent:NSStringFromClass([TableRowSendMail class])
              withParameters:@{@"email" : theRow.email}];
            
        }
        

        
    }
    

}


#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex!=actionSheet.cancelButtonIndex && buttonIndex!=actionSheet.destructiveButtonIndex) {
        
        NSString *numberToDial = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        BOOL error = NO;
        
        NSString *realNumber = [AppSettings extactPhoneNumberToDialFromString:numberToDial error:&error];
        
        if (error) {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Звонок не может быть совершен, так как телефонный номер указан некорректно" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
        } else {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:realNumber]];
            [Flurry logEvent:NSStringFromClass([TableRowDialNumber class])
              withParameters:@{@"phone" : realNumber}];
        }
        
    }
    
}

#pragma mark MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
	[[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:NULL];
}


@end
