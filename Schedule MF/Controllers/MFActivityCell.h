//
//  MFActivityCell.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/30/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MFActivityCellDelegate <NSObject>


@optional
- (void)shareButtonDidTapped:(UITableViewCell *)cell;
- (void)favoritesButtonDidTapped:(UITableViewCell *)cell;

@end

@class MFActivity;

@interface MFActivityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *activityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityTimeHourLabel;
@property (weak, nonatomic) IBOutlet UILabel *activiyTimeMinuteLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityPlaceLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (nonatomic, assign) BOOL expanded;
@property (weak, nonatomic) IBOutlet UILabel *shareLabel;

@property (nonatomic, weak) id<MFActivityCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *favoritesButton;


+ (CGFloat)cellHeight;
- (CGFloat)cellHeightExpanded;
@end
