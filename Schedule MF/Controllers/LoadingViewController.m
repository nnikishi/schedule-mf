//
//  ViewController.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "LoadingViewController.h"
#import "MFActivity+Methods.h"
#import "SlideNavigationController.h"
#import "MenuViewController.h"
#import "ActivityViewController.h"
#import "SlideNavigationContorllerAnimator.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import  "DataManager.h"

@interface LoadingViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, copy) void (^currentCompletionBlock)(void);

@property (nonatomic, strong) SlideNavigationController *slideNavigationController;
@property (nonatomic, strong) MenuViewController *menuViewController;
@property (weak, nonatomic) IBOutlet UIImageView *splashImageView;

@end

@implementation LoadingViewController

#pragma mark getters


- (MenuViewController *)menuViewController
{
    if (!_menuViewController) {
        _menuViewController = [[AppSettings mainStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([MenuViewController class])];
        
    }
    
    return _menuViewController;
}

- (SlideNavigationController *)slideNavigationController
{
    if (!_slideNavigationController) {
        _slideNavigationController = [SlideNavigationController sharedInstance];
        _slideNavigationController.leftMenu = self.menuViewController;
        SlideNavigationContorllerAnimatorSlideAndFade *alideAndFadeAnimator = [[SlideNavigationContorllerAnimatorSlideAndFade alloc] initWithMaximumFadeAlpha:.8 fadeColor:[UIColor clearColor] andSlideMovement:100];
        _slideNavigationController.menuRevealAnimator = alideAndFadeAnimator;
        _slideNavigationController.avoidSwitchingToSameClassViewController = NO;

    }
    
    return _slideNavigationController;
}

#pragma mark UIView LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *launchImage = nil;
    
    if (IS_IPHONE5) {
        launchImage = [UIImage imageNamed:@"splashR4"];
    } else {
        launchImage = [UIImage imageNamed:@"splash"];

    }
    
    self.splashImageView.image = launchImage;
    
    [self loadSettings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Methods
- (void)loadSettings
{
    [self.activityIndicator startAnimating];
    __weak LoadingViewController *wself = self;

    [[AppSettings sharedSettings] getSettingsWithSucessBlock:^{
        
        DLog(@"FEED URL = %@", [AppSettings sharedSettings].feedURL);
        [wself.activityIndicator stopAnimating];
        
        [wself syncActivities];
        
    } errorBlock:^(NSError *error) {
        
        [wself.activityIndicator stopAnimating];

        if (![AppSettings sharedSettings].feedURL) {
            
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[error localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"Повторить", nil) otherButtonTitles:nil];
            [av show];
            
            wself.currentCompletionBlock = ^{[wself loadSettings];};
            
        } else {
        
            [wself syncActivities];

            
        }
        
    }];
    
}

- (void)syncActivities
{
    [self.activityIndicator startAnimating];
    
    __weak LoadingViewController *wself = self;


    [MFActivity syncActivitiesWithSucessBlock:^{
        [wself.activityIndicator stopAnimating];
        [wself addSlideNavigationControllerAsSubview];

    } errorBlock:^(NSError *error) {
        
        [wself.activityIndicator stopAnimating];
        
        NSArray *items = [DataManager fetchDataStore:[DataManager sharedManager].mainObjectContext entity:NSStringFromClass([MFActivity class]) predicate:nil];
        
        if ([items count]>0) {
            DLog(@"Loading from cache");
            [wself addSlideNavigationControllerAsSubview];
        
        } else {
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[error localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"Повторить", nil) otherButtonTitles:nil];
        [av show];
        
        wself.currentCompletionBlock = ^{[wself syncActivities];};

        }
    }];

}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (self.currentCompletionBlock) {
        self.currentCompletionBlock();
        self.currentCompletionBlock = nil;
    }
    
}

- (void)addSlideNavigationControllerAsSubview
{
    if (!_slideNavigationController.view.superview) {
        

       
        
        [self.view addSubview:self.slideNavigationController.view];
        [self addChildViewController:self.slideNavigationController];
        [self.slideNavigationController didMoveToParentViewController:self];
        [self.menuViewController didSelectMenuItemAtIndexPath:self.menuViewController.selectedRow];
    }
}






@end
