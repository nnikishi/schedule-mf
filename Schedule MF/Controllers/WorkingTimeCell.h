//
//  WorkingTimeCell.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/20/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkingTimeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *workingTimeLabel;

@end
