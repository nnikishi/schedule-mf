//
//  WorkingTimeViewController.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 8/20/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "PortraitViewController.h"

@interface WorkingTimeViewController : PortraitViewController

@property (nonatomic, strong) NSString *parameter;
@property (nonatomic, strong) NSString *navBarTitle;


@end
