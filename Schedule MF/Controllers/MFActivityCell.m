//
//  MFActivityCell.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/30/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "MFActivityCell.h"
#import "MFActivity.h"

@implementation MFActivityCell







+ (CGFloat)cellHeight;
{
	CGFloat cellHeight = 59.0;
	return cellHeight;
}

- (IBAction)favoritesButtonDidTapped:(id)sender {
    
    [self.delegate favoritesButtonDidTapped:self];
}
- (IBAction)shareButtonDidTapped:(id)sender {
    [self.delegate shareButtonDidTapped:self];
}

- (CGFloat)cellHeightExpanded
{
    CGFloat cellHeight = 59.0;

    if (self.expanded) {
        cellHeight = MAX(CGRectGetMaxY(self.shareLabel.frame) +5.0, CGRectGetMaxY(self.commentsLabel.frame) + 5.);
    }
    
    return cellHeight;
	
}



@end
