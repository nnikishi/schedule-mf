//
//  MFActivity+Methods.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "MFActivity.h"

@interface MFActivity (Methods)

+ (NSOperation *)syncActivitiesWithSucessBlock:(void (^)())sucessBlock
                                 errorBlock:(void (^)(NSError *error))errorBlock;

+ (NSNumber *)doubleNumberFromTimeString:(NSString *)timeString;

@end
