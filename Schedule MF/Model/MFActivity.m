//
//  MFActivity.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "MFActivity.h"


@implementation MFActivity

@dynamic type;
@dynamic place;
@dynamic category;
@dynamic categoryColor;
@dynamic name;
@dynamic comments;
@dynamic weekDay;
@dynamic startTime;
@dynamic duration;
@dynamic udid;
@dynamic author;
@dynamic isFavorite;

@end
