//
//  MFActivity+Methods.m
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import "MFActivity+Methods.h"
#import "AFNetworking.h"
#import "DataManager.h"

@implementation MFActivity (Methods)

#pragma mark Common

- (NSArray *)allAttributesSorted {
    NSEntityDescription *entityDesc = self.entity;
    NSDictionary *allAttributesDic = [entityDesc attributesByName];
    return [[allAttributesDic allKeys] sortedArrayUsingSelector: @selector(compare:)];
    
}

- (NSArray *)allAttributesSortedFromDic:(NSDictionary *)theDic {
    
    return [[theDic allKeys] sortedArrayUsingSelector: @selector(compare:)];
    
}



+ (NSDictionary *)allObjectsDic:(NSManagedObjectContext *)objectContext {
    
    NSArray *existingObjectsArray = [DataManager fetchDataStore:objectContext entity:@"MFActivity" predicate:nil];
    
    NSMutableDictionary *theDic = [[NSMutableDictionary alloc] init];
    
    for (MFActivity *activity in existingObjectsArray) {
        [theDic setObject:activity forKey:activity.udid];
    }
    
    return theDic;
    
}

- (NSString *)dictionaryHash:(NSDictionary *)dic
{
    NSString *result = nil;
    NSArray *sortedKeys = [self allAttributesSortedFromDic:dic];
    
    for (NSString *key in sortedKeys) {
        
        result = result ? [NSString stringWithFormat:@"%@;%@:%@", result, key, [dic objectForKey:key]] : [NSString stringWithFormat:@"%@:%@", key, [dic objectForKey:key]];
        
    }
    
    return result;
    
}

- (BOOL)compareObjectWithDictionary:(NSDictionary *)theDic
{
    NSString *objectHash = [self coreDataHash:theDic];
    NSString *dicHash = [self dictionaryHash:theDic];
    
    if ([objectHash isEqualToString:dicHash]) {
        return YES;
    }
    
    return NO;
}

- (NSString *)coreDataHash
{
    NSString *result = nil;
    NSArray *sortedKeys = [self allAttributesSorted];
    
    for (NSString *key in sortedKeys) {
        result = result ? [NSString stringWithFormat:@"%@;%@:%@", result, key, [self valueForKey:key]] : [NSString stringWithFormat:@"%@:%@", key, [self valueForKey:key]];
    }
    
    return result;
}

- (NSString *)coreDataHash:(NSDictionary *)theDic
{
    NSString *result = nil;
    NSArray *sortedKeys = [self allAttributesSortedFromDic:theDic];
    
    for (NSString *key in sortedKeys) {
        result = result ? [NSString stringWithFormat:@"%@;%@:%@", result, key, [self valueForKey:key]] : [NSString stringWithFormat:@"%@:%@", key, [self valueForKey:key]];
    }
    
    return result;
}


+ (NSOperation *)syncActivitiesWithSucessBlock:(void (^)())sucessBlock
                                    errorBlock:(void (^)(NSError *error))errorBlock
{
    
    NSURL *url = [AppSettings sharedSettings].feedURL;
    
    if (url) {
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {

                [self saveActivitiesToCoreDataFromJSONObject:responseObject];
            }
            
            sucessBlock();
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            errorBlock(error);
            
        }];
        
        [operation start];
        
        return operation;
    }
    
    NSAssert(url, @"problem with URL");
    return nil;
    

}


+ (void)saveActivitiesToCoreDataFromJSONObject:(id)responseObject

{
    
    __block NSError *theError = nil;
    
    
    NSManagedObjectContext *privateContext = [DataManager sharedManager].privateObjectContext;
    [privateContext performBlockAndWait:^{
        
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *responseDic = (NSDictionary *)responseObject;
            NSArray *array = [[responseDic objectForKey:@"feed"] objectForKey:@"entry"];
            
            
            
            NSDictionary *allObjects = [self allObjectsDic:privateContext];
            NSMutableArray *allKeys = [[NSMutableArray alloc] initWithArray:[allObjects allKeys] copyItems:YES];
            
            
            for (id obj in array) {
                
                if ([obj isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dataDic = (NSDictionary *)obj;
                    
                    NSString *type = [[dataDic objectForKey:@"gsx$type"] objectForKey:@"$t"];
                    NSString *place = [[dataDic objectForKey:@"gsx$place"] objectForKey:@"$t"];
                    NSString *category = [[dataDic objectForKey:@"gsx$category"] objectForKey:@"$t"];
                    NSString *categoryColor = [[dataDic objectForKey:@"gsx$categorycolor"] objectForKey:@"$t"];
                    NSString *name = [[dataDic objectForKey:@"gsx$name"] objectForKey:@"$t"];
                    NSString *comments = [[dataDic objectForKey:@"gsx$comments"] objectForKey:@"$t"];
                    NSNumber *weekDay = [NSNumber numberWithInteger:[[[dataDic objectForKey:@"gsx$weekday"] objectForKey:@"$t"] integerValue]];
                    NSNumber *startTime  = [self doubleNumberFromTimeString:[[dataDic objectForKey:@"gsx$starttime"] objectForKey:@"$t"]];
                    NSNumber *duration = [NSNumber numberWithDouble:[[[dataDic objectForKey:@"gsx$duration"] objectForKey:@"$t"] doubleValue]];
                    NSNumber *udid = [NSNumber numberWithInteger:[[[dataDic objectForKey:@"gsx$udid"] objectForKey:@"$t"] integerValue]];
                    
                    
                    
                    NSDictionary *activityDic = @{@"type": type,
                                                 @"place" : place,
                                                 @"category": category,
                                                 @"categoryColor" : categoryColor,
                                                 @"name" : name,
                                                 @"comments" : comments,
                                                 @"weekDay" : weekDay,
                                                 @"startTime" : startTime,
                                                 @"duration" : duration,
                                                 @"udid" : udid};
                    
                    
                    NSString *key = [activityDic objectForKey:@"udid"];
                    
                    MFActivity *activity = nil;
                    
                    if (key) {
                        activity =[allObjects objectForKey:key];
                    }
                    
                    
                    if (activity) {
                        
                        if (![activity compareObjectWithDictionary:activityDic]) {
                            [activity setValuesForKeysWithDictionary:activityDic];
                            DLog(@"UPDATING CHANNEL %@", activity.udid);
                            
                        }
                        
                        [allKeys removeObject:key];
                        
                    } else {
                        activity = [NSEntityDescription insertNewObjectForEntityForName:@"MFActivity" inManagedObjectContext:privateContext];
                        [activity setValuesForKeysWithDictionary:activityDic];
                        DLog(@"INSERTING CHANNEL %@", activity.udid);
                        
                    }
                    
                    
                }
                
                
                
                
            }
            
            for (NSString *key in allKeys) {
                MFActivity *activityToDelete = [allObjects objectForKey:key];
                [privateContext deleteObject:activityToDelete];
            }
            
            
            [privateContext save:&theError];
            
            
        }
        
    }];
    
    
}

+ (NSNumber *)doubleNumberFromTimeString:(NSString *)timeString
{
    
    NSString *separator = @":";
    
    if ([timeString rangeOfString:separator].location==NSNotFound) {
        separator = @"-";
        
        if ([timeString rangeOfString:separator].location) {
            return @.0;
        }
    }
    
    NSArray *timeComponentsArray = [timeString componentsSeparatedByString:separator];
    
    if ([timeComponentsArray count]!=2) {
        return @.0;
    }
    
    
    NSInteger min = [[timeComponentsArray firstObject] integerValue];
    NSInteger sec = [[timeComponentsArray lastObject] integerValue];
    
    NSInteger time = min*60 + sec;
    
    return [NSNumber numberWithDouble:(double)time];
}




@end
