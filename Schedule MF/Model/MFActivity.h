//
//  MFActivity.h
//  Schedule MF
//
//  Created by Nickolai Nikishin on 7/27/14.
//  Copyright (c) 2014 Nickolai Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MFActivity : NSManagedObject

@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * categoryColor;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSNumber * weekDay;
@property (nonatomic, retain) NSNumber * startTime;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSNumber * udid;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSNumber * isFavorite;

@end
